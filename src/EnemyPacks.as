package{
	import entities.*;
	import org.flixel.*;
	
	import plugins.*;
	/**
	 * ...
	 * @author Cemenyave
	 */
	public class EnemyPacks extends FlxGroup
	{

		
		protected var _spawnX:Number;
		protected var _spawnY:Number;
		
		public var _count:int;
		protected var _numberToSpawn:int;
		
		protected var _spawnInterval:Number;
		
		protected var _avalibleUnits:Array;
		protected var _unit:Class;
		
		public function EnemyPacks(difficulty:Number) {
			super();
			_spawnX = 0;
			_spawnY = 0;
			_numberToSpawn = 3 * difficulty;
			_count = 0;
			
			_spawnInterval = 0.4;
			
			_avalibleUnits = new Array();
			_avalibleUnits.push(Deffkopta);
			_avalibleUnits.push(OrkBommer);
			
			_unit = selectUnit();
		}
		
		public function spawnUnits():void {
            var timer:FlxTimer = new FlxTimer().start(_spawnInterval, _numberToSpawn, this.spawnOnTimer);
        }
		
		protected function spawnOnTimer(Timer:FlxTimer):void {
			var unit:Unit = Registry.$.getFreeObject(_unit) as Unit;
			add(unit);
			Callbacks.addHandler(unit, 'on_entity_terminate', this.removeUnitFromPack);
			unit.spawn(_spawnX, _spawnY);
			_numberToSpawn--;
			_count++;
			//Registry.$.setTTL(t, 15);
		}
		
		protected function selectUnit():Class{
			var multiplyer:int = _avalibleUnits.length - 1; // без выноса расчёта множителя Math.round() глючит.
			return _avalibleUnits[Math.round(Math.random() * multiplyer)];	
		}
		
		protected function moveAlgorithm(unit:Unit):void{
            unit.velocity.x = 0;
            unit.velocity.y = 10;
        }
		
		public function removeUnitFromPack(unit:Entity):void {
			_count--;
			remove(unit);
			Callbacks.removeHandler(unit, 'on_entity_terminate', this.removeUnitFromPack);
		}
		
		override public function update():void 
		{
			super.update();
            if (_count > 0) {
				for each (var unit:FlxObject in members) {
					if (unit is Unit) {
						if(unit.alive) {
                            moveAlgorithm(unit as Unit);
							if (unit.y > FlxG.height)
							{
								(unit as Unit).terminate();
							}
						}
					}
				}
			} else if (_numberToSpawn <= 0) {
				Callbacks.call(this, 'end_of_pack');
			}
		}
	}
}

package
{
	// TODO Добавить Save/Load в Entity
	// TODO Сделать сохранение и загрузку состояния уровня
	// TODO Сделать экраны LVL Complete и LVL Failed
	// TODO Сделать лут из противника
	
	import org.flixel.*;
	import org.flixel.interfaces.*;
	import ui.*;

	public class StatePlay extends FlxState
	{
		// Pseudo - Singleton
		public static function get $():StatePlay {
			return FlxG.state as StatePlay;
		}
		
		public static const COLLISION_SHAPE:uint 	= 1;
		public static const COLLISION_OBJECT:uint 	= 2;
		public static const SFX_OBJECT:uint			= 4;
		public static const UI_OBJECT:uint			= 8;
		
		protected var _current_level_class:Class;
		
		protected var _collision_shape:FlxGroup;
		protected var _collision_objects:FlxGroup;
		protected var _collision_objects_to_layer:Array;
		protected var _ui_objects:FlxGroup;
		protected var _sfx_objects:FlxGroup;
		
		public function StatePlay(LevelClass:Class) {
			super();
			_current_level_class = LevelClass;
		}
		
		override public function create():void
		{
			FlxG.score 						= 0;
			FlxG.bgColor 					= 0xff18181b;
			FlxG.mouse.hide();
			
			_collision_shape 				= new FlxGroup();
			_collision_objects 				= new FlxGroup();
			_collision_objects_to_layer		= new Array();
			_sfx_objects 					= new FlxGroup();
			_ui_objects 					= new FlxGroup();
			
			add(new _current_level_class());
			add(_collision_shape);
			add(_collision_objects);
			add(_sfx_objects);
			add(_ui_objects);
			
			var _pause_menu:UI 				= new MenuPause().hide();
			_ui_objects.add(_pause_menu);
			FlxG.lostFocusCallback 			= _pause_menu.show;
			FlxG.gotFocusCallback 			= function():void;
		}
		
		override public function destroy():void
		{	
			super.destroy();
			_collision_shape 				= null;
			_collision_objects 				= null;
			_sfx_objects 					= null;
			_ui_objects 					= null;
		}
		
		override public function update():void
		{
			// Меню
			if (FlxG.paused) {
				_ui_objects.update();
				return;
			}
			
			super.update();
			
			for (var s:* in _collision_objects.members) {
				if (s in _collision_objects_to_layer && _collision_objects_to_layer[s] == false)
					continue;
				else
					FlxG.collide(_collision_shape, _collision_objects.members[s]);
			}
			for (var k:* in _collision_objects.members) {
				if (k == 0)
					continue;
				for (var n:* in _collision_objects.members) {
					if (n <= k)
						continue;
					FlxG.overlap(_collision_objects.members[k], _collision_objects.members[n], overlapped);
				}
			}
		}
		
		protected function overlapped(Sprite1:*, Sprite2:*):void {
			if (!(Sprite1 is Entity) || !(Sprite2 is Entity))
				return;
			if (!(Sprite2 as Entity).alive || !(Sprite2 as Entity).alive)
				return;
			if (!(Sprite2 as Entity).collidable || !(Sprite2 as Entity).collidable)
				return;
				
			(Sprite1 as Entity).doHurt(Sprite2);
			(Sprite2 as Entity).doHurt(Sprite1);
			
			return;
		}
		
		public function addObject(obj:FlxBasic, layers:uint, team:uint = 0):FlxBasic {
			if (layers & COLLISION_SHAPE) {
				_collision_shape.add(obj);
			}
			if (layers & SFX_OBJECT) {
				_sfx_objects.add(obj);
			}
			if (layers & UI_OBJECT) {
				_ui_objects.add(obj);
			}
			if (layers & COLLISION_OBJECT) {
				while (_collision_objects.members.length <= team) {
					_collision_objects.add(new FlxGroup());
				}	
				_collision_objects.members[team].add(obj);
			}
			return obj;
		}
		
		public function removeObject(obj:FlxBasic, layers:uint, team:uint = 0):void {
			if (layers & COLLISION_SHAPE) {
				_collision_shape.remove(obj, true);
			}
			if (layers & SFX_OBJECT) {
				_sfx_objects.remove(obj, true);
			}
			if (layers & UI_OBJECT) {
				_ui_objects.remove(obj, true);
			}
			if (layers & COLLISION_OBJECT) {
				if (!(team in _collision_objects.members))
					return;
				_collision_objects[team].members.remove(obj, true);
			}
		}
		
		public function getNearestEnemy(obj:Entity):Entity {
			var group:uint = getEntityTeam(obj);
			var obj_point:FlxPoint = new FlxPoint(obj.x, obj.y);
			var nearest:Entity;
			var dist:Number;
			for (var k:* in _collision_objects.members) {
				if (k == 0)
					continue;
				if (k == group)
					continue;
				if (!nearest) {
					nearest = getNearestGroupMember(obj_point, _collision_objects.members[k]);
				} else {
					var t_nearest:Entity = getNearestGroupMember(obj_point, _collision_objects.members[k]);
					if (t_nearest) {
						var t_dist:Number = FlxU.getDistance(obj_point, new FlxPoint(t_nearest.x, t_nearest.y));
						if (t_dist < dist) {
							dist = t_dist;
							nearest = t_nearest;
						}
					}
				}
			}
			return nearest;
		}
		
		public function getNearestGroupMember(point:FlxPoint, group:FlxGroupInterface, nearest:Entity = null):Entity {
			if (!('members' in group))
				return nearest;
			var dist:Number;
			if (nearest)
				dist = FlxU.getDistance(point, new FlxPoint(nearest.x, nearest.y));
			for each(var member:* in group['members']) {
				if (member is FlxGroupInterface) {
					nearest = getNearestGroupMember(point, member, nearest);
				}
				if (member is FlxObject) {
					var t_dist:Number = FlxU.getDistance(point, new FlxPoint(member.x, member.y));
					if (!nearest || t_dist < dist) {
						nearest = member;
						dist = t_dist;
					}
				}
			}
			return nearest;
		}
		
		public function getEntityTeam(obj:Entity):uint {
			_collision_objects;
			for (var k:* in _collision_objects.members) {
				if(inGroup(obj,_collision_objects.members[k]))
					return k;
			}
			return 0;
		}
		
		public function setTeamLayerCollision(team:uint, collide:Boolean = true):void {
			if (!(team in _collision_objects.members))
				return;
			_collision_objects_to_layer[team] = collide;
		}
		
		public function inGroup(obj:Entity, group:FlxGroup):Boolean {
			if (group.members.indexOf(obj) >= 0)
				return true;
			for each (var member:* in group.members) {
				if (member is FlxGroup) {
					if (inGroup(obj, member))
						return true;
				}
			}
			return false;
		}
	}
}

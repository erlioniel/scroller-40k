package waves
{
	import org.flixel.*;
	import entities.*;
	
	public class BossWave extends EnemyPacks {
        /**
         * @param difficulty
         */
		public function BossWave(difficulty:Number){

            super(difficulty);
			FlxG.log('BossWave');
            _avalibleUnits = new Array();
            _avalibleUnits.push(OrkBossFlightFortress);
            _numberToSpawn = 1;
			_unit = OrkBossFlightFortress;
		}

        /**
         * @param Timer
         */
		override protected function spawnOnTimer(Timer:FlxTimer):void {
            FlxG.log('Spawn boss');
			_spawnY = 32;
			_spawnX = FlxG.width / 2;
			super.spawnOnTimer(Timer);
		}

        /**
         * @param unit
         */
		override protected function moveAlgorithm(unit:Unit):void {
			if (unit.y < 50){
				unit.velocity.x = 0;
				unit.velocity.y = 10;
			}else if (unit.velocity.y > 0){
				unit.acceleration.y = -3;
			}else {
				unit.velocity.y = 0;
			}
		}
    }
}
package waves 
{
	import entities.Deffkopta;
	
	import flash.utils.getTimer;
	
	import org.flixel.*;
	/**
	 * ...
	 * @author Cemenyave
	 */
	public class LinarLeftToRightWave extends EnemyPacks
	{
		
		
		public function LinarLeftToRightWave(difficulty:Number)
		{
			super(difficulty);
		}
		
		override public function spawnUnits():void 
		{
			_spawnX = 100;
			_spawnY = 100;
			super.spawnUnits();
		}
		
		override protected function spawnOnTimer(Timer:FlxTimer):void 
		{
			super.spawnOnTimer(Timer);
		}
		
		override protected function moveAlgorithm(unit:Unit):void
		{
			
			unit.velocity.x = 100 * Math.sin((getTimer() - unit.spawnTime)/600);
			if(Math.abs(unit.velocity.x) < 50){
				unit.velocity.y = 30;
			} else {
				unit.velocity.y = 1;
			}
		}
	}
}
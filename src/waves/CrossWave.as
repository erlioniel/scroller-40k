package waves 
{
	import entities.*;
	
	import flash.utils.getTimer;
	
	import org.flixel.*;
	/**
	 * ...
	 * @author Cemenyave
	 */
	public class CrossWave extends EnemyPacks 
	{
		
		protected var isOdd:Boolean;
		
		public function CrossWave(difficulty:Number) 
		{
			super(difficulty);
			isOdd = true;
			_spawnInterval = 0.3
				
		}
		
		override protected function spawnOnTimer(Timer:FlxTimer):void 
		{
			if(isOdd){
				_spawnX = 50;
				_spawnY = 50;
			} else {
				_spawnY = 50;
				_spawnX = FlxG.width - 50;
			}
			isOdd = !isOdd;
			super.spawnOnTimer(Timer);
		}
		
		override protected function moveAlgorithm(unit:Unit):void
		{
			unit.velocity.y = 10;
			if (unit.x < 36)
			{
				unit.velocity.x += 3;
			} else if (unit.x >= FlxG.width - 32)
			{
				unit.velocity.x -= 3;	
			}
		}
	}

}
package ui 
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class MenuMainBackground extends UI
	{
		[Embed(source = "data/hud_mainmenu.png")] protected var ImgHudMenu:Class;
		
		public function MenuMainBackground() 
		{
			add(new FlxSprite(18, 0, ImgHudMenu));
		}
		
	}

}
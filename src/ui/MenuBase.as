package ui 
{
	import adapters.*;
	import plugins.*;
	import org.flixel.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class MenuBase extends UI
	{
		[Embed(source = "data/cursor.png")] public var ImgCursor:Class;
		[Embed(source = "data/hud_levelselect_btn.png")] protected var ImgHudLevelSelect:Class;
		[Embed(source = "data/hud_levelselect_img.png")] protected var ImgHudLevelImg:Class;
		[Embed(source = "data/hud_mainmenu_btn.png")] protected var ImgHudMenuBtn:Class;
		[Embed(source = "data/hud_mainmenu_btn_sfx.png")] protected var ImgHudMenuBtnSfx:Class;
		[Embed(source = "data/hud_mainmenu_btn_music.png")] protected var ImgHudMenuBtnMusic:Class;
		
		protected var _button_label_normal:int;
		protected var _button_label_hover:int;
		protected var _button_label_fontsize:int;
		protected var _button_imgclass:Class;
		
		public function MenuBase() {
			super();
			
			FlxG.mouse.show(ImgCursor, 2);
			
			_align = LEFT + TOP;
			
			_button_label_normal = 0xffdde2f6;
			_button_label_hover = 0xaadde2f6;
			_button_label_fontsize = 8;
			_button_imgclass = ImgHudMenuBtn;
			
			add(new MenuMainBackground());
			
			// SPONSORS
			add(createInvisibleButton(new FlxRect(absX(32),absY(160),56,80), btnURL, ["http://goo.gl/JIUT2"]));
			add(createInvisibleButton(new FlxRect(absX(32), absY(244), 92, 12), btnURL, ["http://pixelgame.ru"]));
			
			// SOUND 
			var _musicBtn:FlxNiceButton = new FlxNiceButton(absX(95), absY(159), musicSwitch, [], null, 47, 27);
			_musicBtn.loadGraphic(ImgHudMenuBtnMusic, 1, true, [0], [1],[3]);
			_musicBtn.stateAdd("off", 1, true, [2], [3],[1]);
			_musicBtn.stateSelect(SoundMixer.$.getMuteChannel(1) ? "off" : "default");
			add(_musicBtn);
			
			var _sfxBtn:FlxNiceButton = new FlxNiceButton(absX(95), absY(191), sfxSwitch, [], null, 47, 27);
			_sfxBtn.loadGraphic(ImgHudMenuBtnSfx, 1, true, [0], [1],[3]);
			_sfxBtn.stateAdd("off", 1, true, [2], [3],[1]);
			_sfxBtn.stateSelect((FlxG.mute ? "off" : "default"));
			add(_sfxBtn);
		}
		
		override protected function createButton(label:String, layout:FlxRect, callback:Function = null, ...rest):FlxBasic {
			var btn:FlxNiceButton = new FlxNiceButton(layout.x, layout.y, callback, rest, label, layout.width, layout.height);
			
			btn.loadGraphic(_button_imgclass,1,true,[0],[1]);
			btn.textNormal
				.setFormat(null, _button_label_fontsize,_button_label_normal, "center", 0xff000000);
			btn.textHighlight
				.setFormat(null, _button_label_fontsize,_button_label_hover, "center", 0xff000000);
			btn.pauseProof = true;
			
			return btn as FlxBasic;
		}
		
		protected function musicSwitch(btn:FlxNiceButton):void {
			if (btn.stateCurrent == "default") {
				btn.stateSelect("off");
			}
			else btn.stateSelect();
			SoundMixer.$.muteChannel(1, !SoundMixer.$.getMuteChannel(1));
		}
		
		protected function sfxSwitch(btn:FlxNiceButton):void {
			if (btn.stateCurrent == "default") {
				btn.stateSelect("off");
			}
			else btn.stateSelect();
			FlxG.mute = !FlxG.mute;
		}
		
	}

}
package ui 
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class MenuMain extends MenuBase
	{
		
		public function MenuMain() 
		{
			//add(new MenuBase());
			
			var screen_about:UI = new MenuMainAbout();
			add(screen_about.hide());
			var screen_help:UI = new MenuMainHelp();
			add(screen_help.hide());
			
			add(createButton("Play", new FlxRect(absX(18), absY(63), 124, 27), btnStateChange, StatePlay, Level));
			add(createButton("About", new FlxRect(absX(18), absY(63 + 32), 124, 27), btnSwitchGroup, screen_about, [screen_help]));
			add(createButton("Help", new FlxRect(absX(18), absY(63 + 64), 124, 27), btnSwitchGroup, screen_help, [screen_about]));
		}
		
	}

}
package ui 
{
	import adapters.*;
	import org.flixel.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class PlayerHUD extends UI
	{
		[Embed(source = "data/hud_ingame_panel.png")] protected var ImgHudPanel:Class;
		[Embed(source = "data/hud_ingame_lifes.png")] protected var ImgHudLifes:Class;
		
		protected var _score:FlxText;
		protected var _health:Array;
		protected var _unit:Entity;
		
		public function PlayerHUD(unit:Entity) 
		{
			_align = TOP + LEFT;
			
			_unit = unit;
			
			add(new FlxSprite(absX(0),absY(0))
				.loadGraphic(ImgHudPanel));
				
			_score = new FlxText(absX(286), absY(1), 60, "000000000");
			_score.color = 0xffdbff00;
			add(_score);
			
			_health = new Array();
			for (var i:int = 0; i < 16; i++ ) {
				var health_sprite:FlxSprite = new FlxSprite(absX(5 + i * 6), absY(4));
				health_sprite.loadGraphic(ImgHudLifes, true, false);
				health_sprite.addAnimation("full",[0]);
				health_sprite.addAnimation("half",[1]);
				health_sprite.addAnimation("null",[2]);
				health_sprite.addAnimation("noup", [3]);
				_health.push(health_sprite);
				add(health_sprite);
			}
			
		}
		
		public function updateHealth():void {
			var hp:Number = _unit.health;
			for (var i:* in _health) {
				if (hp >= (i + 1) * 10) _health[i].play("full");
				else if (hp > i * 10) _health[i].play("half");
				else if (_unit.maxHealth > i * 10) _health[i].play("null");
				else _health[i].play("noup");
			}
		}
		
		override public function update():void 
		{
			if (FlxG.score < 0) {
				FlxG.score = 0;
			}
			_score.text = FlxUtils.addZero(FlxG.score + "", 9);
			updateHealth();
			super.update();
		}
		
		override public function destroy():void 
		{
			super.destroy();
			for (var k:* in _health) {
				_health[k] = null;
			}
			_health = null;
			_score = null;
			_unit = null;
		}
	}

}
package ui 
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class MenuMainHelp extends UI
	{
		
		public function MenuMainHelp() 
		{
			super();
			
			_width = 176;
			_x = -20;
			_y = 20;
			_align = TOP + RIGHT;
			
			add(new FlxText(absX(), absY(0), _width, "HELP")
				.setFormat(null,16,0xffff84,"left"));
			add(new FlxText(absX(), absY(32), _width, "A,D - Move \nW, SPACE - Jump \nMOUSE - Aiming & shoot \nE - Activate btn & etc")
				.setFormat(null, 8, 0x888888, "left"));
			add(new FlxText(absX(), absY(128), _width, "TIPS")
				.setFormat(null, 16, 0xffff84, "left"));
			add(new FlxText(absX(), absY(160), _width, "Look around! Maybe you will find something helpful to complete the level!")
				.setFormat(null, 8, 0x888888, "left"));
		}
		
	}

}
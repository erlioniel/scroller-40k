package ui 
{
	import org.flixel.*;
	import plugins.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class MenuPause extends MenuBase
	{
		[Embed(source = "data/hud_mainmenu_pause.png")] protected var ImgHudMenuPause:Class;
		
		public function MenuPause() 
		{
			add(createButton("Continue", new FlxRect(absX(18), absY(63), 124, 27), btnSwitchGroup, this));
			add(createButton("Return to Menu", new FlxRect(absX(18), absY(63 + 32), 124, 27), btnStateChange, StateMenu));
			add(new FlxSprite(absX(24), absY(8), ImgHudMenuPause));
		}
		
		override public function show(mask:Boolean = false):UI 
		{
			FlxG.paused = true;
			SoundMixer.$.switchChannel(1, 0);
			FlxG.mouse.show(ImgCursor, 2);
			return super.show(true);
		}
		
		override public function hide():UI 
		{
			FlxG.paused = false;
			SoundMixer.$.switchChannel(0, 1);
			FlxG.mouse.hide();
			return super.hide();
		}
		
		override public function update():void 
		{
			super.update();
			if (FlxG.keys.justPressed("ESCAPE")) {
				if (!visible) {
					show();
				} else {
					hide();
				}
			}
		}
	}

}
package ui 
{
	import adapters.*;
	import org.flixel.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class UI extends FlxGroup
	{
		protected const MIDDLE:int		= 1;
		protected const CENTER:int		= 2;
		protected const LEFT:int		= 4;
		protected const RIGHT:int		= 8;
		protected const TOP:int			= 16;
		protected const BOTTOM:int		= 32;
		
		protected var _x:int;
		protected var _y:int;
		protected var _width:int;
		protected var _height:int;
		protected var _align:int;
		protected var _close_for_esc:Boolean;
		
		protected var _scrolling:Boolean;
		
		protected var _mask:FlxSprite;
		protected var _mask_color:int;
		
		protected var _state_change:Class;
		protected var _state_change_array:Array;
		
		public function UI() {
			_x = 0;
			_y = 0;
			_width = 0;
			_height = 0;
			_align = CENTER + MIDDLE;
			_close_for_esc = false;
			_scrolling = false;
			
			_mask = new FlxSprite(0, 0);
			_mask.visible = false;
			_mask.alive = false;
			add(_mask);
			_mask_color = 0xcc000000;
		}
		
		protected function maskShow():void {
			if (!_mask.alive) {
				_mask.makeGraphic(FlxG.width, FlxG.height, _mask_color);
			}
			_mask.visible = true;
			_mask.alive = true;
		}
		
		// PUBLIC
		
		public function show(mask:Boolean = false ):UI {
			visible = true;
			alive = true;
			if (mask) {
				maskShow();
			}
			return this;
		}
		
		public function hide():UI {
			visible = false;
			alive = false;
			return this;
		}
		
		public function setup():UI {
			return this;
		}
		
		public function absZero():FlxPoint {
			var point:FlxPoint = new FlxPoint(_x,_y);
			if (_align & CENTER) {
				point.x += FlxG.width / 2 - _width / 2;
			} else if (_align & RIGHT) {
				point.x += FlxG.width - _width;
			}
			if (_align & MIDDLE) {
				point.y += FlxG.height / 2 - _height / 2;
			} else if (_align & BOTTOM) {
				point.y += FlxG.height - _height;
			}
			return point;
		}
		
		public function absX(local_x:int = 0):int {
			var point:FlxPoint = absZero();
			return point.x + local_x;
		}
		
		public function absY(local_y:int = 0):int {
			var point:FlxPoint = absZero();
			return point.y + local_y;
		}
		
		protected function createButton(label:String, layout:FlxRect, callback:Function = null, ... rest):FlxBasic {
			var btn:FlxNiceButton = new FlxNiceButton(layout.x, layout.y, callback, rest, label);
			return btn as FlxBasic;
		}
		
		protected function createInvisibleButton(layout:FlxRect, callback:Function = null, ... rest):FlxBasic {
			var btn:FlxNiceButton = new FlxNiceButton(layout.x, layout.y, callback, rest, "", layout.width, layout.height);
			btn.makeGraphic(layout.width, layout.height, 0x00000000);
			return btn as FlxBasic;
		}
		
		protected function btnURL(btn:FlxBasic, url:String = null):void {
			if (!url) {
				throw new Error ("You must set url for btnUrl");
			}
			FlxU.openURL(url);
		}
		
		protected function btnStateChange(btn:FlxBasic, new_state:Class, ... rest):void {
			_state_change = new_state;
			_state_change_array = rest;
			FlxG.fade(0xff000000, 0.3, btnStateChangeFade);
		}
		
		protected function btnStateChangeFade():void {
			var state:FlxState;
			var rest:Array = _state_change_array;
			
			if (rest.length <= 0)			state = new _state_change();
			else if (rest.length <= 1)		state = new _state_change(rest[0]);
			else if (rest.length <= 2)		state = new _state_change(rest[0],rest[1]);
			else if (rest.length <= 3)		state = new _state_change(rest[0],rest[1],rest[2]);
			else if (rest.length <= 4)		state = new _state_change(rest[0],rest[1],rest[2],rest[3]);
			else if (rest.length <= 5)		state = new _state_change(rest[0],rest[1],rest[2],rest[3],rest[4]);
			else if (rest.length <= 6)		state = new _state_change(rest[0],rest[1],rest[2],rest[3],rest[4],rest[5]);
			else if (rest.length <= 7)		state = new _state_change(rest[0],rest[1],rest[2],rest[3],rest[4],rest[5],rest[6]);
			else if (rest.length <= 8)		state = new _state_change(rest[0],rest[1],rest[2],rest[3],rest[4],rest[5],rest[6],rest[7]);
			else if (rest.length <= 9)		state = new _state_change(rest[0],rest[1],rest[2],rest[3],rest[4],rest[5],rest[6],rest[7],rest[8]);
			else throw new Error("Button proxy don't support too much arguments");
			
			for (var k:* in _state_change_array) {
				_state_change_array[k] = null;
			}
			_state_change = null;
			_state_change_array = null;
			
			FlxG.switchState(state);
		}
		
		protected function btnSwitchGroup(btn:FlxBasic, window:UI, close_window:Array = null):void {
			if (window.visible) {
				window.hide();
			} else {
				window.show();
				if (!close_window) {
					return;
				}
				for (var k:* in close_window) {
					if (close_window[k] is UI) {
						(close_window[k] as UI).hide();
					}
				}
			}
		}
		
		// OVERRIDES
		
		override public function update():void 
		{
			super.update();
			if (_close_for_esc && FlxG.keys.justPressed("ESCAPE")) {
				hide();
			}
		}
		
		override public function destroy():void 
		{
			super.destroy();
		}
		
		override public function add(Object:FlxBasic):FlxBasic 
		{
			if (Object is FlxObject && !_scrolling) {
				(Object as FlxObject).scrollFactor = new FlxPoint();
				(Object as FlxObject).cameras = [FlxG.camera];
			}
			return super.add(Object);
		}
	}

}
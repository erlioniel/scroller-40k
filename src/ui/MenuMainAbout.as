package ui 
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class MenuMainAbout  extends UI
	{
		
		public function MenuMainAbout() 
		{
			super();
			
			_width = 176;
			_x = -20;
			_y = 20;
			_align = TOP + RIGHT;
			
			add(new FlxText(absX(), absY(0), _width, "ABOUT")
				.setFormat(null,16,0xffff84,"left"));
			add(new FlxText(absX(), absY(32), _width, "Game create by Dmitry Dryga\nDmitry Chepuryshkin\nand Vladimir Kryukov\n\n(c) 2012 Pixelgame.ru")
				.setFormat(null, 8, 0x888888, "left"));
			add(new FlxText(absX(), absY(128), _width, "THANKS")
				.setFormat(null, 16, 0xffff84, "left"));
			add(new FlxText(absX(), absY(160), _width, "Adam Saltsman, for Flixel\nAdobe, for Adobe Flash\nMika Palmu, for FlashDevelop")
				.setFormat(null, 8, 0x888888, "left"));
		}
		
	}

}
package  
{
	import org.flixel.*;
	import plugins.Registry;
	import sfx.BaseInterface;
	import adapters.*;
	
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class SFX extends FlxGroup
	{
		protected var _timeToPlay:int;
		protected var _timeToPlayDefault:int;
		protected var _local:Boolean;
		
		public function SFX(local:Boolean = false) {
			super();
			_local = local;
			if(!_local) {
				_timeToPlayDefault = 1000;
			}
		}
		
		public function playEffect(X:int, Y:int):void {
			alive = true;
			visible = true;
			if (_timeToPlay <= 0) {
				_timeToPlay = _timeToPlayDefault;
			}
			if (!_local) {
				StatePlay.$.addObject(this, StatePlay.SFX_OBJECT);
			}
			for (var k:* in members) {
				if (members[k] is SFX) {
					(members[k] as SFX).playEffect(X, Y);
					continue;
				}
				if ('customPlay' in members[k] && members[k]['customPlay'] is Function) {
					members[k]['customPlay'](X, Y);
					continue;
				}
				if (!(members[k] is BaseInterface)) {
					continue;
				}
				var obj:BaseInterface = members[k] as BaseInterface;
				obj.playEffect(X, Y);
			}
		}
		
		public function stopEffect():void {
			alive = false;
			visible = false;
			if (!_local) {
				StatePlay.$.removeObject(this, StatePlay.SFX_OBJECT);
			}
			for (var k:* in members) {
				if (members[k] is SFX) {
					(members[k] as SFX).stopEffect();
					continue;
				}
				if ('customStop' in members[k] && members[k]['customStop'] is Function) {
					members[k]['customStop']();
					continue;
				}
				if (!(members[k] is BaseInterface)) {
					continue;
				}
				var obj:BaseInterface = members[k] as BaseInterface;
				obj.stopEffect();
			}
		}
		
		public function sets(... rest):void { }
		
		public static function play(X:int, Y:int, Effect:Class, ... rest):void {
			var effect:SFX = Registry.$.getFreeObject(Effect) as SFX;
			effect.sets.apply(null, rest);
			effect.playEffect(X, Y);
			Registry.$.setTTL(effect, 20);
		}
		
		override public function update():void 
		{
			super.update();
			if (alive && !_local) {
				if (_timeToPlay > 0) {
					_timeToPlay -= FlxG.elapsed*1000;
				} else {
					_timeToPlay = 0;
					stopEffect();
				}
			}
		}
		
		override public function destroy():void 
		{
			if (!_local) {
				Registry.$.removeObject(this);
			}
			super.destroy();
		}
	}

}
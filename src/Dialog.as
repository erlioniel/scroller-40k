package  
{
	import org.flixel.*
	import entities.*;
	
	/**
	 * TODO Сделать озвучку вывода текста
	 */
	
	public class Dialog extends FlxGroup
	{
		// SINGLETON
		protected static var _me:Dialog;
		
		public static function get D():Dialog {
			if (!_me) _me = new Dialog();
			return _me;
		}
		public static function set D(value:Dialog):void { return; }
		
		[Embed(source = "data/hud_dialogbox.png")] protected var ImgHudDialogbox:Class;
		
		protected var _box:FlxSprite;
		protected var _sprite:FlxSprite;
		protected var _textFrom:FlxText;
		protected var _textValue:FlxText;
		protected var _textValueString:String;
		protected var _timer:Number = 0;
		
		protected var _on:Boolean = false;
		
		protected var _tasks:Array;
		
		protected var _fadeIn:Number = 0.3;
		
		public function Dialog() {
			super();
			
			visible = false;
			
			_tasks = new Array();
			
			_sprite = new FlxSprite();
			_sprite.alpha = 0;
			_sprite.makeGraphic(1, 1, 0x00000000);
			
			_box = new FlxSprite(0, 197, ImgHudDialogbox);
			_box.alpha = 0;
			add(_box);
			
			_textFrom = new FlxText(91, 202, FlxG.width - 102, "");
			_textFrom.setFormat(null, 8, 0xfafa95, "left");
			_textFrom.alpha = 0;
			_textValue = new FlxText(91, 212, FlxG.width - 102, "");
			_textValue.setFormat(null, 8, 0x888888, "left");
			_textValue.alpha = 0;
			
			add(_textFrom);
			add(_textValue);
			add(_sprite);
			
			setAll("scrollFactor", new FlxPoint());
			
			StatePlay._state._hud.add(this);
		}
		
		override public function update():void 
		{
			super.update();
			if (_timer > 0) {
				visible = true;
				if (_textValue.text.length < _textValueString.length) _textValue.text = _textValueString.substr(0, _textValue.text.length+1);
				if (FlxG.keys.justPressed("SPACE") || FlxG.keys.justPressed("ENTER")) _timer = -1;
				if (_box.alpha >= 1) {
					_timer -= FlxG.elapsed / FlxG.timeScale;
				}
				// FadeIn
				else {
					setAll("alpha", _box.alpha + (FlxG.elapsed / FlxG.timeScale) * 1 / _fadeIn);
				}
			}
			else if (_timer < 0) {
				if (_box.alpha <= 0) {
					visible = false;
					_timer = 0;
					Player.P.freeze = false;
				}
				// FadeOut
				else {
					setAll("alpha", _box.alpha - (FlxG.elapsed / FlxG.timeScale) * 1 / _fadeIn);
				}
			}
			if (_on && !visible) {
				_say.apply(this, _tasks.shift());
				if (_tasks.length <= 0) _on = false;
			}
		}
		
		override public function destroy():void 
		{
			super.destroy();
			_me = null;
			_textFrom = null;
			_textValue = null;
			_box = null;
			_sprite = null;
		}
		
		public function _say(from:String = "Anonymous", text:String = "...", sprite:Class = null, spriteParams:Array = null, freeze:Boolean = false, delay:Number = 5, color:uint = 0xfafa95):void {
			_textFrom.text = from;
			_textValue.text = "";
			_textValueString = text;
			_textFrom.color = color;
			_timer = delay;
			if (sprite) {
				_sprite.loadGraphic(sprite, false, false, spriteParams[0],spriteParams[1]);
				_sprite.scale = new FlxPoint(4, 4);
				_sprite.x = 48 + (spriteParams[2] ? spriteParams[2] : 0);
				_sprite.y = 200 + (spriteParams[3] ? spriteParams[3] : 0);
			}
			if (freeze) Player.P.freeze = true;
		}
		
		/**
		 * 
		 * @param	from
		 * @param	text
		 * @param	sprite
		 * @param	spriteParams	0 - width, 1 - height, 2 - offsetX, 3 - offsetY
		 * @param	delay
		 * @param	color
		 */
		public static function say(from:String = "Anonymous", text:String = "...", sprite:Class = null, spriteParams:Array = null, freeze:Boolean = false, delay:Number = 5, color:uint = 0xfafa95):void {
			Dialog.D._tasks.push([from, text, sprite, spriteParams, freeze, delay, color]);
			Dialog.D._on = true;
		}
	}
}
package  
{
	/**
	 * Original TaskManager by Romanko Denis
	 * @author Romanko Denis (Stormit) http://xitri.com
	 * @version 3.0
	 */
	
	import org.flixel.*;
	
	public class UnitAI extends FlxBasic
	{
		protected var tasks:Array;
		protected var pauseCount:Number = 0;
		protected var isStarted:Boolean = false;
		protected var isPaused:Boolean = false;
		protected var result:Boolean;
		public var cycle:Boolean;
		
		
		/**
		 * Constructor
		 * @param cycle Boolean value indicating whether or not use cycle mode.
		 * 
		 */	
		public function UnitAI(cycle:Boolean = false) {
			this.cycle = cycle;
			tasks = [];
		}
		/**
		 * 
		 * @param	_obj
		 */
		override public function update():void 
		{
			super.update();
			if (isStarted && !isPaused) {
				var curTask:Object = tasks[0] as Object;
				if (curTask) {
					result = (curTask.func as Function).apply(this, curTask.params);
					if(curTask == tasks[0] && (curTask.isInstant || result)) {
						nextTask(curTask.ignoreCycle);
					}
				}
			}
		}
		
		/**
		 * Add task to the end of queue. This function will called by ENTER_FRAME event yet will not return true. 
		 * @param func Function that will called
		 * @param params Array of parameters that will passed to function
		 * @param ignoreCycle Boolean value. If true function will call once (it makes sense if cycle=true)
		 * 
		 */		
		public function addTask(func:Function, params:Array = null, ignoreCycle:Boolean = false):void {
			tasks.push({func:func, params:params, ignoreCycle:ignoreCycle});
			start();
		}
		
		/**
		 * Add task to the end of queue. This function will called once. After that the following task will be carried out.
		 * @param func Function that will called
		 * @param params Array of parameters that will passed to function
		 * @param ignoreCycle Boolean value. If true function will call once (it makes sense if cycle=true)
		 * 
		 */
		public function addInstantTask(func:Function, params:Array = null, ignoreCycle:Boolean = false):void {
			tasks.push({func:func, params:params, ignoreCycle:ignoreCycle, isInstant:true});
			start();
		}
		
		/**
		 * Add task and makes it first in queue. This function will called by ENTER_FRAME event yet will not return true. 
		 * @param func Function that will called
		 * @param params Array of parameters that will passed to function
		 * @param ignoreCycle Boolean value. If true function will call once (it makes sense if cycle=true)
		 * 
		 */	
		public function addUrgentTask(func:Function, params:Array = null, ignoreCycle:Boolean = false):void{
			tasks.unshift({func:func, params:params, ignoreCycle:ignoreCycle});
			start();
		}
		
		/**
		 * Add task and makes it first in queue. This function will called once. After that the following task will be carried out.
		 * @param func Function that will called
		 * @param params Array of parameters that will passed to function
		 * @param ignoreCycle Boolean value. If true function will call once (it makes sense if cycle=true)
		 * 
		 */
		public function addUrgentInstantTask(func:Function, params:Array = null, ignoreCycle:Boolean = false):void{
			tasks.unshift({func:func, params:params, ignoreCycle:ignoreCycle, isInstant:true});
			start();
		}
		
		/**
		 * Add pause between tasks 
		 * @param t Quantity of ms which will last a pause
		 * @param ignoreCycle Boolean value. If true function will call once (it makes sense if cycle=true)
		 * 
		 */			
		public function addPause(t:int, ignoreCycle:Boolean = false, urgent:Boolean = false):void {
			if (urgent) addUrgentTask(pauseTask, [t], ignoreCycle);
			else addTask(pauseTask, [t], ignoreCycle);
		}
		
		/**
		 * @private
		 * Function that implements a pause task. Adds to queue when addPause() is caused.
		 * @param t int The internal counter
		 * 
		 */		
		protected function pauseTask(t:int):void {
			pauseCount += Math.floor(FlxG.elapsed*1000);
			if(pauseCount >= t) {
				pauseCount = 0;
				nextTask();
			}
		}
		
		public function currentPause():Boolean {
			var curTask:Object = tasks[0] as Object;
			if (curTask.func == pauseTask) return true;
			return false;
		}
		
		/**
		 * Remove all tasks from queue. In fact stops the TaskManager object. 
		 * 
		 */		
		public function removeAllTasks():void {
			tasks = [];
		}
		
		/**
		 * Passes to the next task. Usually this function is caused automatically when the current task returns result true. But you can cause it compulsorily if that is demanded by the logic of your application.
		 * @param ignoreCycle Boolean value. If true current task will not be added to the end of queue (it makes sense if cycle=true)
		 * 
		 */		
		public function nextTask(ignoreCycle:Boolean = false):void {
			if(cycle && !ignoreCycle) {
				tasks.push(tasks.shift());
			} else {
				tasks.shift();
			}
		}
		
		/**
		 * @private 
		 * Starts TaskManager. It is caused when the new task is added.
		 */		
		protected function start():void {
			if(!isStarted) {
				isStarted = true;
				isPaused = false;
			}
		}
		
		/**
		 * @private 
		 * Stop TaskManager. It is caused when the last task return true.
		 */	
		protected function stop():void {
			isStarted = false;
		}
		
		/**
		 * Set on/off pause.
		 * Contains boolean value
		 * 
		 */		
		public function get pause():Boolean {
			return isPaused;
		}
			
		public function set pause(value:Boolean):void {
			if(value && !isPaused) {
				isPaused = true;
			} else if(tasks[0] && isPaused) {
				isPaused = false;
			}
		}
		
		/**
		 * Read only.
		 * Contains quantity of tasks in queue.
		 * 
		 */		
		public function get lenght():int {
			return tasks.length;
		}
	}

}
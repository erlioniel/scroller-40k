package plugins
{
	import org.flixel.*;
	import flash.utils.*;
	
	/**
	 * Класс служащий для регистрации потоковых объектов (т.н. стример),
	 * и выборки объектов которые можно использовать в данный момент.
	 * Экономит память, фпс и нервы!
	 * 
	 * @author	Vladimir Kryukov
	 */
	public class Registry extends FlxBasic
	{
		// Pseudo - Singleton
		public static function get $():Registry {
			return FlxG.getPlugin(Registry) as Registry;
		}
		
		// Registry array
		protected var _registry:Array;
		protected var _registry_ttl:Array;
		protected var _registry_default_ttl:int;
		protected var _timer:FlxTimer;
		protected var _clearTimer:int;
		protected var _clearPerCycle:int;
		
		// Constructor
		public function Registry() {
			_clearPerCycle = 5;
			_clearTimer = 30;
			_registry_default_ttl = 0;
			
			_registry = new Array();
			_registry_ttl = new Array();
			
			_timer = new FlxTimer().start(_clearTimer, 1, this.clearRegistryTimer);
		}
		
		// PROTECTED
		
		protected function getClassRegistry(className:String):Array {
			if (!_registry[className]) {
				_registry[className] = new Array();
			}
			if (!_registry_ttl[className]) {
				_registry_ttl[className] = new Array();
			}
			return _registry[className];
		}
		
		protected function addObject(obj:*):void {
			getClassRegistry(FlxU.getClassName(obj)).push(obj);
		}
		
		protected function clearRegistry():void {
			for (var reg:* in _registry) {
				clearObjectReq(reg, 0, -9999, true);
				_registry[reg] = null;
				_registry_ttl[reg] = null;
			}
		}
		
		protected function clearRegistryTimer(t:FlxTimer):void {
			for (var reg:* in _registry_ttl) {
				clearObjectReq(reg);
			}
			_timer = new FlxTimer().start(_clearTimer, 1, this.clearRegistryTimer);
		}
		
		protected function clearObjectReq(reg:String, k:int = 0, i:int = 0, hard:Boolean = false):void {
			if (!_registry[reg] || !_registry[reg][k]) {
				return;
			}
			if (_clearPerCycle > 0 && i >= _clearPerCycle) {
				return;
			}
			i++;
			if ((_registry_ttl[reg][k] > 0 && _registry_ttl[reg][k] < getTimer()) || hard) {
				if ('destroy' in _registry[reg][k] && _registry[reg][k]['destroy'] is Function) {
					_registry[reg][k]['destroy']();
				}
				removeObject(_registry[reg][k]);
			} else {
				k++;
			}
			return clearObjectReq(reg, k, i);
		}
		
		protected function existsObject(obj:*):Boolean {
			if (!obj) return false;
			var classname:String = FlxU.getClassName(obj);
			if (classname in _registry && _registry[classname].indexOf(obj) >= 0) {
				return true;
			}
			return false;
		}
		
		// PUBLIC
		
		/**
		 * Функция для установки TimeToLive объекта. Если TTL не установлен - объект не будет уничтожаться вообще
		 * @param	obj		Объект к которому устанавливается TTL
		 * @param	ttl		Время жизни в СЕКУНДАХ с момента установки (фактически может жить дольше)
		 */
		public function setTTL(obj:*, ttl:int = 0):void {
			var reg:Array = getClassRegistry(FlxU.getClassName(obj));
			_registry_ttl[FlxU.getClassName(obj)][reg.indexOf(obj)] = getTimer() + ttl*1000;
		}
				
		/**
		 * Функция ищет первый объект который удовлетворяет условию checkField == true
		 * @param	objectClass		Класс объекта, который надо найти
		 * @param	checkField		Имя параметра, которы является проверочным. По умолчанию "alive"
		 * @return					Объект, который можно использовать. Если объекта нет - то он автоматически создается
		 */
		public function getFreeObject(objectClass:Class, checkField:String = "alive"):* {
			var reg:Array = getClassRegistry(FlxU.getClassName(objectClass));
			//FlxG.watch(reg, "length", "Reg:"+FlxU.getClassName(objectClass));
			for each(var obj:* in reg) {
				if (obj is objectClass && !obj[checkField]) {
					if (obj is FlxBasic && (obj as FlxBasic).destroyed) {
						obj.destroy();
						removeObject(obj);
						return getFreeObject(objectClass, checkField);
					}
					return obj;
				}
			}
			return getNewObject(objectClass);
		}
		
		/**
		 * Возращает новый объект определенного класса и записывает его в регистр
		 * @param	objectClass		Класс, объект которого нужно создать
		 * @return					Новый объект
		 */
		public function getNewObject(objectClass:Class):* {
			var new_obj:* = new objectClass();
			addObject(new_obj);
			if (_registry_default_ttl != 0) {
				setTTL(new_obj, _registry_default_ttl);
			}
			return (new_obj as objectClass);
		}
		
		/**
		 * Удаляет объект из регистра
		 * @param	obj		Объект, который необходимо найти и удалить
		 */
		public function removeObject(obj:*):void {
			if (!existsObject(obj)) {
				return;
			}
			var reg:Array = getClassRegistry(FlxU.getClassName(obj));
			_registry_ttl[FlxU.getClassName(obj)].splice(reg.indexOf(obj),1);
			reg.splice(reg.indexOf(obj),1);
		}
		
		/**
		 * Удаляет класс (и все объекты) из регистра
		 * @param	objectClass		Класс, который необходимо удалить
		 */
		public function removeObjectClass(objectClass:Class):void {
			var reg:Array = getClassRegistry(FlxU.getClassName(objectClass));
			for each(var obj:* in reg) {
				obj = null;
			}
			_registry[FlxU.getClassName(objectClass)] = null;
			_registry_ttl[FlxU.getClassName(objectClass)] = null;
		}
		
		/**
		 * Эксперементальная функия. Вызывает определенный метод у полученного через getFreeObject объекта
		 * @param	objectClass		Класс объекта для поиска
		 * @param	func			Имя функции для вызова (функция должна быть public)
		 * @param	... args		Аргументы, которые будут переданны в функцию
		 * @return					Результат работы функции
		 */
		public function callFreeObject(objectClass:Class, func:String,  ... args):* {
			var obj:* = getFreeObject(objectClass);
			if (!obj[func] || !(obj[func] is Function)) {
				return false;
			}
			return (obj[func] as Function).apply(null,args);
		}
		
		// OVERRIDE
		
		override public function destroy():void 
		{
			clearRegistry();
			super.destroy();
		}
	}

}
package plugins
{
	import org.flixel.*;
	import adapters.FlxSoundGeti;
	
	/**
	 * Класс-плагин для flixel-а, реализующий простой микшер на несколько дорожек
	 * для управления фоновой музыкой. Класс поддерживает цепочки вызовов
	 * @author		Vladimir Kryukov
	 * @version		0.1
	 */
	public class SoundMixer extends FlxBasic
	{
		protected var _channels:Array;
		protected var _channels_volume:Array;
		protected var _channels_mute:Array;
		protected var _mixer_volume:Number;
		protected var _mixer_mute:Boolean;
		
		// Pseudo - Singleton
		public static function get $():SoundMixer {
			return FlxG.getPlugin(SoundMixer) as SoundMixer;
		}
		
		/**
		 * Конструктор класса, вызывается автоматически при добавлении плагина в игру
		 * @param	channels	Количество каналов которые необходимо создать
		 * @param	global		Нужно ли сделать этот экземпляр глобальным (глобальный экзепляр может быть только один и будет доступен по SoundMixer.i)
		 */
		public function SoundMixer(channels:int = 2) {
			_channels = new Array();
			_channels_volume = new Array();
			_channels_mute = new Array();
			_mixer_volume = 1;
			_mixer_mute = false;
			
			while (channels--) {
				addChannel();
			}
		}
		
		/**
		 * Переключение с канала на канал. Останавливает первый канал и запускает второй
		 * @param	from		ID канала для остановки
		 * @param	to			ID канала для запуска
		 * @param	pause		Нужно ли трек в первом канале поставить на паузу (иначе сработает функция stop)
		 * @param	fade		Время плавного заглушения в секундах (0 для резкого перехода)
		 * @return				Объект плагина
		 */
		public function switchChannel(from:int, to:int, pause:Boolean = true, fade:int = 1):SoundMixer {
			if (_channels[from] && _channels[from].playing) {
				stopChannel(from, pause, fade);
			}
			if (_channels[to]) {
				playChannel(to, fade);
			}
			return this;
		}
		
		/**
		 * Остановка трека, который на данный момент играет в канале
		 * @param	channel		ID канала, в котором нужно остановить трек
		 * @param	pause		Поставить трек на паузу или остановить
		 * @param	fade		Время плавного заглушения трека в секундах
		 * @return				Объект плагина
		 */
		public function stopChannel(channel:int, pause:Boolean = false, fade:int = 0):SoundMixer {
			if (fade) {
				_channels[channel].fadeOut(fade, pause);
			} else if (pause) {
				_channels[channel].pause();
			} else {
				_channels[channel].stop();
			}
			return this;
		}
		
		/**
		 * Остановить все дорожки
		 * @param	pause		Поставить на паузу или остановить
		 * @param	fade		Время плавного заглушения треков в секундах
		 * @return				Объект плагина
		 */
		public function stopAllChannel(pause:Boolean = false, fade:int = 0):SoundMixer {
			for (var i:* in _channels) {
				stopChannel(i, pause, fade);
			}
			return this;
		}
		
		/**
		 * Загрузить трек в дорожку. Для загрузки трек должен быть присоединен как embedded
		 * Если канал занят - то трек в канале будет остановлен и заменен на новый
		 * @param	channel		ID канала, в который загрузить трек
		 * @param	embed		Embedded класс трека, который нужно загрузить
		 * @param	loop		Должен ли трек зацикливаться после проигрывания
		 * @param	frames		Длинна трека в фреймах для бесшовного цикла. Вычисляется как [частота_трека]*[время_в_секундах]
		 * @return				Объект плагина
		 */
		public function loadChannel(channel:int, embed:Class = null, loop:Boolean = false, frames:int = 0):SoundMixer {
			if (_channels[channel]) {
				if (_channels[channel].playing) {
					stopChannel(channel);
				}
				_channels[channel].loadEmbedded(embed, loop, frames);
			}
			return this;
		}
		
		/**
		 * Запускает выбранную дорожку
		 * @param	channel		ID дорожки которую нужно запустить
		 * @param	fade		Время заглушения дорожки в секундах
		 * @param	force		Принудительный рестарт дорожки (если она была поставлена на паузу)
		 * @return				Объект плагина
		 */
		public function playChannel(channel:int, fade:int = 0, force:Boolean = false):SoundMixer {
			if (force) {
				stopChannel(channel);
			}
			if (fade) {
				_channels[channel].fadeIn(fade);
			} else {
				_channels[channel].play();
			}
			return this;
		}
		
		/**
		 * Устанавливает уровень громкости для дорожки
		 * @param	channel		ID дорожки для настройки
		 * @param	volume		Громкость (от 0 до 1) для установки
		 * @return				Объект плагина
		 */
		public function volumeChannel(channel:int, volume:Number = 1):SoundMixer {
			if (_channels[channel]) _channels_volume[channel] = volume;
			return this;
		}
		
		/**
		 * Устанавливает уровень громкости для всех дорожек
		 * @param	volume		Громкость (от 0 до 1) для установки
		 * @return				Объект плагина
		 */
		public function volumeAllChannel( volume:Number = 1 ):SoundMixer {
			for (var i:* in _channels) {
				volumeChannel(i, volume);
			}
			return this;
		}
		
		/**
		 * Устанавливает общую громкость для микшера (не меняет громкость дорожек)
		 * @param	volume		Громкость (от 0 до 1) для установки
		 * @return				Объект плагина
		 */
		public function volumeMixer(volume:Number = 1):SoundMixer {
			_mixer_volume = volume;
			return this;
		}
		
		/**
		 * Отключает / включает звук на дорожке
		 * @param	channel		ID дорожки для отключения звука
		 * @param	mute		Отключить или включить звук
		 * @return				Объект плагина
		 */
		public function muteChannel(channel:int, mute:Boolean = true):SoundMixer {
			if (_channels[channel]) _channels_mute[channel] = mute;
			return this;
		}
		
		/**
		 * Отключает / включает звук на всех дорожках
		 * @param	mute		Отключить или включить звук
		 * @return				Объект плагина
		 */
		public function muteAllChannel( mute:Boolean = true):SoundMixer {
			for (var i:* in _channels) {
				muteChannel(i, mute);
			}
			return this;
		}
		
		/**
		 * Отключает / включает звук на микшере
		 * @param	mute		Отключить или включить звук
		 * @return				Объект плагина
		 */
		public function muteMixer(mute:Boolean = true):SoundMixer {
			_mixer_mute = mute;
			return this;
		}
		
		// GET FUNCTIONS
		
		/**
		 * Получает текущее значение громкости на дорожке
		 * @param	channel		ID дорожки для получения уровня громкости
		 * @return				Уровень громкости от 0 до 1
		 */
		public function getVolumeChannel(channel:int):Number {
			if (!_channels[channel]) {
				return 0;
			}
			return _channels_volume[channel];
		}
		
		/**
		 * Получает текущее значение заглушения дорожки
		 * @param	channel		ID дорожки для получения заглушения
		 * @return				true / false
		 */
		public function getMuteChannel(channel:int):Boolean {
			if (!_channels[channel]) {
				return false;
			}
			return _channels_mute[channel];
		}
		
		/**
		 * Добавляет новую дорожку в микшер
		 * @return				ID новой дорожки
		 */
		public function addChannel():int {
			var l:int = _channels.length;
			_channels.push(new FlxSoundGeti());
			_channels_mute.push(false);
			_channels_volume.push(1.0);
			return l;
		}
		
		// OVERRIDING
		
		override public function destroy():void 
		{
			for (var i:* in _channels) {
				_channels[i].destroy;
				_channels[i] = null;
			}
			_channels = null;
			_channels_mute = null;
			_channels_volume = null;
			
			super.destroy();
		}
		
		override public function update():void 
		{
			for (var i:* in _channels) {
				if (_channels[i].playing) {
					_channels[i].update();
					if (_channels_mute[i] || _mixer_mute) {
						_channels[i].volume = 0;
					} else {
						_channels[i].volume = _mixer_volume*_channels_volume[i];
					}
				}
			}
			super.update();
		}
	}

}
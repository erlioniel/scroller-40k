package sfx
{
	import org.flixel.*;
	import plugins.*;
	
	public class BaseSprite extends FlxSprite implements BaseInterface
	{
		public var randomFacing:Boolean = true;
		public var randomAngle:Boolean = true;
		
		public function BaseSprite() {
			super();
			
			visible = false;
			alive = false;
			solid = false;
		}
		
		override public function update():void 
		{
			super.update();
			
			if (alive && finished) {
				stopEffect();
			}
		}
		
		public function playEffect(X:int, Y:int):void {
			if (randomFacing) {
				facing = FlxG.random() > 0.5 ? RIGHT : LEFT;
			}
			if (randomAngle) {
				angle = FlxG.random() * 360;
			}
			alive = true;
			visible = true;
			play("play");
			x = X - width/2;
			y = Y - height/2;
		}
		
		public function stopEffect():void {
			alive = false;
			visible = false;
		}
	}

}
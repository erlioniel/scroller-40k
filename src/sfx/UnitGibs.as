package sfx 
{
	
	public class UnitGibs extends SFX
	{
		[Embed(source = "data/particles_bot.png")] protected var ImgParticlesBot:Class;
		
		protected var _emitter:BaseEmitter;
		
		public function UnitGibs(local:Boolean = false) {
			super(local);
			
			_emitter = new BaseEmitter();
			_emitter.makeParticles(ImgParticlesBot, 10, 8, true, 0);
			_emitter.lifespan = 2;
			add(_emitter);
		}
		
		override public function playEffect(X:int, Y:int):void 
		{
			_emitter.emitTimer = 100;
			super.playEffect(X, Y);
		}
		
		override public function destroy():void 
		{
			_emitter = null;
			super.destroy();
		}
	}

}
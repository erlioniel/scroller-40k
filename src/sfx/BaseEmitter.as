package sfx
{
	import org.flixel.*;
	
	public class BaseEmitter extends FlxEmitter implements BaseInterface
	{
		public var emitTimer:int = 0;
		public var explosion:Boolean;
		
		public function BaseEmitter(solid:Boolean = false) {
			super();
			
			setXSpeed(-50,50);
			setYSpeed(-200,0);
			setRotation(-720,-720);
			gravity = 350;
			bounce = 0.5;
			lifespan = 1;
			explosion = false;
			frequency = 0.01;
			
			setAll("solid", solid, true);
		}
		
		override public function update():void 
		{
			super.update();
			
			if (on) {
				if (emitTimer > 0) {
					emitTimer -= FlxG.elapsed*1000;
				}
				else {
					on = false; 
				}
			} else {
				stopEffect();
			}
		}
		
		public function playEffect(X:int, Y:int):void {
			this.x = X;
			this.y = Y;
			start(explosion,lifespan,frequency);
		}
		
		public function stopEffect():void {
			alive = false;
		}
	}

}
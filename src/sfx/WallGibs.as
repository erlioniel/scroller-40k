package sfx 
{
	import org.flixel.*;
	
	public class WallGibs extends SFX
	{
		[Embed(source = "data/particles_wall.png")] protected var ImgParticlesWall:Class;
		[Embed(source = "data/sound_wall.mp3")] protected var SoundWallHit:Class;
		
		public function WallGibs(local:Boolean = false) {
			super(local);
			
			var _emitter:BaseEmitter = new BaseEmitter();
			_emitter.makeParticles(ImgParticlesWall, 20, 8, true, 0);
			_emitter.lifespan = 1;
			add(_emitter);
			
			var _sound:BaseSound = new BaseSound();
			_sound.loadEmbedded(SoundWallHit);
			add(_sound);
		}
	}

}
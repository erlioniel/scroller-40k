package sfx 
{
	import org.flixel.FlxSound;
	import plugins.*;
	public class UnitBoom extends SFX
	{
		[Embed(source = "data/boom_1.png")] protected var ImgBoom1:Class;
		[Embed(source = "data/boom.mp3")] protected var SoundBoom:Class;
		
		public function UnitBoom(local:Boolean = false) {
			super(local);
			
			_timeToPlayDefault = 2000;
			
			// SOUND
			var _sound:BaseSound = new BaseSound();
			_sound.loadEmbedded(SoundBoom);
			add(_sound);
			
			add(new UnitGibs(true));
			
			// SPRITE
			var _sprite_boom:BaseSprite = new BaseSprite();
			_sprite_boom.loadGraphic(ImgBoom1, true, false, 32, 32);
			_sprite_boom.addAnimation("play", [0, 1, 2, 3, 4, 5, 6, 7], 24, false);
			add(_sprite_boom);
		}
	}

}
package sfx 
{
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class BaseSound extends FlxSound implements BaseInterface
	{
		public function BaseSound() { }
		
		public function playEffect(X:int, Y:int):void {
			x = X;
			y = Y;
			play(true);
		}
		
		public function stopEffect():void {
			stop();
		}
	}

}
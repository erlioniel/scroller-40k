package sfx 
{
	import org.flixel.*;
	
	public class TextDamage extends SFX
	{
		protected var _text:BaseText;
		
		public function TextDamage(local:Boolean = false) {
			super(local);
			
			_text = new BaseText();
			_text.color = 0xffff0000;
			_text.size = 8;
			_text.width = 10;
			add(_text);
		}
		
		override public function playEffect(X:int, Y:int):void 
		{
			_text.timeToPlay = 1000 * 0.2;
			super.playEffect(X, Y);
		}
		
		override public function destroy():void 
		{
			_text = null;
			super.destroy();
		}
		
		override public function sets(...rest):void 
		{
			_text.text = "" + rest.shift();
			super.sets();
		}
	}
}
package sfx
{
	import org.flixel.*;
	import plugins.*;
	
	public class BaseText extends FlxText implements BaseInterface
	{
		public var timeToPlay:int;
		
		public function BaseText() 
		{
			super(0, 0, 100);
			visible = false;
		}
		
		override public function update():void 
		{
			super.update();
			
			if (alive) {
				if (timeToPlay > 0) {
					timeToPlay -= FlxG.elapsed*1000;
				} else if (alpha > 0) {
					alpha -= FlxG.elapsed*5;
					velocity.y -= 20;
				} else {
					stopEffect();
				}
			}
		}
		
		public function playEffect(X:int, Y:int):void {
			if (text) {
				visible = true;
				alive = true;
				alpha = 1;
				this.x = X - width/2;
				this.y = Y - height/2;
				velocity.y = 0;
			}
		}
		
		public function stopEffect():void {
			alpha = 0;
			alive = false;
		}
	}

}
package
{
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import plugins.*;
	
	// Физические размеры и другие настройки флешки
	[SWF(width="704", height="512", backgroundColor="#000000")]
	[Frame(factoryClass="Preloader")]

	public class Scroller40k extends FlxGame
	{
		public function Scroller40k():void
		{
			super(352,256,StateMenu,2,50,50);
			forceDebugger = true;
			FlxG.debug = true;
			FlxG.addPlugin(new SoundMixer());
			FlxG.addPlugin(new Registry());
		}
	}
}

package
{
	import org.flixel.*;
	import ui.*;

	public class StateMenu extends FlxState
	{
		public var _hud:UI;
		
		override public function create():void
		{	
			FlxG.lostFocusCallback = function():void;
			FlxG.gotFocusCallback =  function():void;
			
			_hud = new MenuMain();
			
			add(_hud);
		}
		
		override public function destroy():void
		{
			super.destroy();
			_hud = null;
		}
	}
}

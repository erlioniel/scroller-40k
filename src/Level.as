package  
{
	import adapters.*;
	import flash.display.BitmapData;
	import org.flixel.*;
	import entities.*;
	import ui.*;
	import plugins.*;
	
	public class Level extends FlxGroup
	{
		public static var MenuText:String = "Default Empty Level";
		
		[Embed(source = "data/map_grass.png")] public var ImgMapGrass:Class;
		[Embed(source = "data/map_grass2.png")] public var ImgMapGrass2:Class;
		[Embed(source = "data/map_grass3.png")] public var ImgMapGrass3:Class;
		
		[Embed(source = "data/map_clouds.png")] public var ImgMapClouds:Class;
		[Embed(source = "data/map_sea.png")] public var ImgMapSea:Class;
		
		// http://soundcloud.com/affordableaudio_music_01/military-dogfight-loop
		[Embed(source = "data/sound/sample.mp3")] public var SoundTheme:Class;
		
		public function Level() {
			super();
			
			generateFrame(FlxG.width,FlxG.height);
			
			var player:Entity = new Player().spawn((FlxG.width / 2), (FlxG.height * 0.7));
			StatePlay.$.addObject(player, StatePlay.COLLISION_OBJECT, 1);
			StatePlay.$.addObject(new PlayerHUD(player), StatePlay.UI_OBJECT);
			
			StatePlay.$.addObject(new Waves().startNextWave(), StatePlay.COLLISION_OBJECT, 2);
			StatePlay.$.setTeamLayerCollision(2, false);
			
			var sea_back:Backgound = new Backgound();
			sea_back.add(new FlxSprite(0, 0, ImgMapSea));
			sea_back.add(new FlxSprite(0, 0, ImgMapSea));
			sea_back.start();
			sea_back.scroll_multi = 0.5;
			add(sea_back);
			
			var grass_back:Backgound = new Backgound();
			grass_back.add(new FlxSprite(0, 0, ImgMapGrass));
			grass_back.add(new FlxSprite(0, 0, ImgMapGrass2));
			grass_back.add(new FlxSprite(0, 0, ImgMapGrass3));
			grass_back.start();
			add(grass_back);
			
			var clouds_back:Backgound = new Backgound();
			clouds_back.add(new FlxSprite(0, 0, ImgMapClouds));
			clouds_back.add(new FlxSprite(0, 0, ImgMapClouds));
			clouds_back.start();
			clouds_back.scroll_multi = 1.5;
			add(clouds_back);
			
			SoundMixer.$
				.loadChannel(1, SoundTheme, true, (112.015 * 44100))
				.playChannel(1,1,true);
		}
		
		protected function generateFrame(frame_width:int, frame_height:int):void
		{
			FlxG.camera.setBounds(0,0,frame_width,frame_height,true);
			
			StatePlay.$.addObject(new FlxTileblock(0,0,frame_width,5), StatePlay.COLLISION_SHAPE);
			StatePlay.$.addObject(new FlxTileblock(0,0,5,frame_height), StatePlay.COLLISION_SHAPE);
			StatePlay.$.addObject(new FlxTileblock(0,frame_height-5,frame_width,5), StatePlay.COLLISION_SHAPE);
			StatePlay.$.addObject(new FlxTileblock(frame_width-5,0,5,frame_height), StatePlay.COLLISION_SHAPE);
		}
	}

}
package  
{
	import adapters.FlxSpriteLayer;
	import org.flixel.*;
	import flash.utils.getTimer;

	/**
	 * Базовый класс Entity для практически всех объектов в игре
	 * 
	 * @author	Vladimir Kryukov
	 */
	public class Entity extends FlxSpriteLayer
	{	
		/**
		 * PROTECTED VARS
		 * Данные настройки используются в объектах наследниках чтобы частично менять поведение объекта
		 */
		
		/**
		 * Случайный флип при спавне энтити
		 */
		protected var _spawnRandomFacing:Boolean = false;
		/**
		 * Будет ли объект неуязвим после столкновения
		 */
		protected var _hurtFlick:Boolean = false;
		/**
		 * Время которое объект будет мигать после столкновения
		 */
		protected var _hurtFlickTime:int = 0;
		/**
		 * Базовые повреждения которые нанесет объект при столкновении
		 * Для более сложного поведения может быть переопределена функция-getter для damageHurt
		 */
		protected var _hurtDamage:int = 0;
		
		/**
		 * PUBLIC VARS
		 * Общедоступные переменные и параметры
		 */
		
		/**
		 * Может ли этот объект коллизится с геометрией уровня
		 * Разделение физических объектов и объектов с которыми может взаимодействовать игрок (тоже solid, но не collidable)
		 */
		public var collidable:Boolean = false;
		/**
		 * Будет ли юнит получать повреждения
		 */
		public var invulnerable:Boolean = false;
		/**
		 * getTimer() времени в которое Entity была заспавнена
		 */
		public var spawnTime:int;
		/**
		 * Счет, который будет добавлен за Hurt данного объекта
		 */
		public var scoreHurt:int = 0;
		/**
		 * Счет, который будет добавлен за убийство данного объекта
		 */
		public var scoreKill:int = 0;
		/**
		 * Максимальное количество жизней энтити
		 * Можно установить принудительно задав жизни перед спавном юнита
		 */
		public var maxHealth:int = 0;
		/**
		 * Ограничение движения объекта
		 */
		public var freeze:Boolean = false;
		
		/**
		 * PUBLIC METHODS
		 */
		
		public function Entity() 
		{
			super(0,0);
			
			health = 100;
			
			immovable = true;
			
			alive = false;
			solid = false;
			exists = false;
		}
		
		/**
		 * Перегрузка метода update для того чтобы работать с анимацией
		 */
		override public function update():void
		{
			super.update();
			
			if (!alive && exists) {
				if (finished)	
					terminate();
				return;
			}
			
			animateMove();
		}
		
		/**
		 * Спавнит объект
		 * @return				this
		 */
		public function spawn(spawn_x:Number = 100, spawn_y:Number = 100):Entity {
			if (health > maxHealth)
				maxHealth = health;
			else
				health = maxHealth;
			if (_spawnRandomFacing)
				facing = FlxG.random() > 0.5 ? RIGHT : LEFT;
			x = spawn_x - width / 2;
			y = spawn_y - height / 2;
			visible = true;
			alive = true;
			solid = true;
			exists = true;
			spawnTime = getTimer();
			animateSpawn();
			return this
		}
		
		/**
		 * DEPRICATED!
		 * Функция аннигилирована в пользу более информативных doHurt и haveHurt
		 * @param	Damage		Повреждения которые необходимо нанести
		 */
		override public function hurt(Damage:Number):void {
			super.hurt(Damage);
		}
		
		/**
		 * Функция нанесения столкновения с другой Entity
		 * Базовый функционал - вызов haveHurt другой энтити с повреждениями
		 * @param	obj			Целевой объект
		 * @return				this
		 */
		public function doHurt(obj:Entity):Entity {
			if(!flickering)
				obj.haveHurt(this, hurtDamage);
			return this;
		}
		
		/**
		 * Функция повреждений от столкновений с другой Entity
		 * Базовый функционал - нанесение повреждений
		 * @param	obj			Объект с которым произошло столкновение
		 * @param	damage		Повреждения которые объект пытается нанести
		 * @return				true если повреждения по объекту прошли
		 */
		public function haveHurt(obj:Entity, damage:int = 0):Boolean {
			if (!alive || invulnerable)
				return false;
			if (flickering && _hurtFlick)
				return false;
			flicker(_hurtFlickTime);
			if (doDamage(damage)) 
				obj.addScore(scoreKill);
			else
				obj.addScore(scoreHurt);
			return true;
		}
		
		/**
		 * Нанесение повреждений объекту
		 * @param	damage		Сколько повреждений сделать
		 * @return				TRUE если объект умер в результате нанесения повреждений
		 */
		public function doDamage(damage:Number):Boolean {
			if (!alive) 
				return false;
			health -= damage;
			if (damage > 0)
				animateDamage(damage);
			if (health <= 0) {
				kill();
				return true;
			}
			return false;
		}
		
		/**
		 * Убийство объекта
		 */
		override public function kill():void {
			if (!alive || !exists)
				return; 
			alive = false;
			solid = false;
			spawnTime = 0;
            finished = true;
			animateKill();
			Callbacks.call(this, 'on_entity_kill');
		}
		
		/**
		 * Удаляет объект из игры. В некотором роде является аналогом функции kill
		 * Используется для тихого удаления ненужных объектов со сцены (к примеру юнитов улетевших за край экрана)
		 * @return					this
		 */
		public function terminate():Entity {
			alive = false;
			solid = false;
			super.kill();
			Callbacks.call(this, 'on_entity_terminate');
			return this;
		}
		
		/**
		 * Функция добавления счета. Вызывается для объекта к которому надо добавить счет
		 * @param	score		Счет для добавления
		 * @return				this
		 */
		public function addScore(score:Number):Entity {
			FlxG.score += score;
			return this;
		}
		
		/**
		 * Равномерное движение в определенном направлении
		 * @param	speed		Скорость движения
		 * @param	angle		Угол
		 * @return				this
		 */
		public function moveAngle(speed:Number = 0, angle:Number = 0):Entity {
			if (freeze) return this;
			velocity.x = (speed ? speed : maxVelocity.x) * Math.sin(angle / 180*Math.PI);
			velocity.y = - (speed ? speed : maxVelocity.y) * Math.cos(angle / 180 * Math.PI);
			if (velocity.x > 0) 
				facing = RIGHT;
			else if (velocity.x < 0)
				facing = LEFT;
			return this;
		}
		/**
		 * Двигаться наверх
		 * @param	speed		Скорость движения
		 * @return				this
		 */
		public function moveUp(speed:Number = 0):Entity {
			return moveAngle(speed, 0);
		}
		
		/**
		 * Двигаться вниз
		 * @param	speed		Скорость движения
		 * @return				this
		 */
		public function moveDown(speed:Number = 0):Entity {
			return moveAngle(speed, 180);
		}
		
		/**
		 * Двигаться влево
		 * @param	speed		Скорость движения
		 * @return				this
		 */
		public function moveLeft(speed:Number = 0):Entity {
			return moveAngle(speed, -90);
		}
		
		/**
		 * Двигаться вправо
		 * @param	speed		Скорость движения
		 * @return				this
		 */
		public function moveRight(speed:Number = 0):Entity {
			return moveAngle(speed, 90);
		}
		
		/**
		 * Двигаться в сторону определенной точки
		 * @param	speed		Скорость движения
		 * @param	point		Точка в сторону которой двигаться
		 * @return				this
		 */
		public function movePoint(point:FlxPoint, speed:Number = 0):Entity {
			return moveAngle(speed, FlxU.getAngle(new FlxPoint(x, y), point));
		}
		
		/**
		 * PROTECTED METHODS
		 */
		
		/**
		 * Анимация спавна
		 */
		protected function animateSpawn():void {
			playIfExists("spawn");
		}
		
		/**
		 * Анимация ожидания
		 */
		protected function animateIdle():void {
			playIfExists("idle");
		}
		
		/**
		 * Анимация получения повреждений
		 */
		protected function animateDamage(damage:Number):void {
			playIfExists("damaged");
		}
		
		/**
		 * Анимация смерти
		 */
		protected function animateKill():void {
			playIfExists("kill");
		}
		
		/**
		 * Анимация движения
		 */
		protected function animateMove():void {
			if (animationExists("move_down") && velocity.y > 0)
				play("move_down");
			else if (animationExists("move_up") && velocity.y < 0)
				play("move_up");
			else if(animationExists("move_forward") && velocity.x != 0) {
				if (animationExists("move_backward")) {
					if (velocity.x > 0 && facing == LEFT)
						play("move_backward");
					else if (velocity.x < 0 && facing == RIGHT)
						play("move_backward");
					else
						play("move_forward");
				} else {
					play("move_forward");
				}
			}
			else 
				animateIdle();
		}
		
		/**
		 * Повреждения наносимые энтити при столкновении
		 * Оформлено в виде функции для того чтобы можно было изменить поведение у наследников
		 */
		protected function get hurtDamage():int {
			return _hurtDamage;
		}
		protected function set hurtDamage(damage:int):void {
			_hurtDamage = damage;
		}
	}

}

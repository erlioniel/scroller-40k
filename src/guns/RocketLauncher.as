package guns 
{
	import org.flixel.FlxObject;
	/**
	 * ...
	 * @author Dryga Dmitry
	 */
	public class RocketLauncher extends Gun 
	{
		[Embed(source = "../data/bullet_rocket.png")] protected var ImgBulletRocket:Class;
		
		public function RocketLauncher(object:Unit) 
		{
			super(object);
			
			_damage = 20;
			fireRate = 2500;
			speed = 70;
			_bulletTTL = 5000;
			
			_bulletGraphic = ImgBulletRocket;
		}
	}

}
package guns 
{
	import org.flixel.FlxObject;
	/**
	 * ...
	 * @author Dryga Dmitry
	 */
	public class Machinegun extends Gun
	{
		[Embed(source = "../data/bullet_fire.png")] protected var ImgBulletSimple:Class;
		
		public function Machinegun(object:Unit) 
		{
			super(object);
			
			_damage = 2;
			fireRate = 300;
			speed = 100;
			angleRandom = 10;
			_bulletTTL = 2000;
			
			_bulletGraphic = ImgBulletSimple;
		}
		
	}

}
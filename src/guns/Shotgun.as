package guns 
{
	import org.flixel.FlxObject;
	
	/**
	 * ...
	 * @author Dryga Dmitry
	 */
	public class Shotgun extends Gun 
	{
		[Embed(source = "../data/bullet_fire_green.png")] protected var ImgBulletSimple:Class;
		/**
		 * Создание нового ружья
		 * @param	object Юнит к которому привязано новое ружьишко
		 */
		public function Shotgun(object:Unit) 
		{
			super(object);
			
			_damage = 2;
			fireRate = 500;
			speed = 200;
			angleRandom = 30;
			_bulletTTL = 500;
			_bulletTTLRandom = 100;
			_multishot = 5;
			_speedRandom = 100;
			
			_bulletGraphic = ImgBulletSimple;
		}
		
	}

}
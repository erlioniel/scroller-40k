package guns 
{
	import adapters.FlxDevFile;
	import org.flixel.FlxSprite;
	import org.flixel.FlxG;
	
	import plugins.Registry;
	
	/**
	 * ...
	 * @author Dmitry Dryga
	 */
	public class Bullet extends Entity 
	{
		private var _damage:Number;
		private var _damagePerSecond:Number;
		private var _enemy:Boolean;
		private var _damagedObjects:Object;
		private var _ttl:Number;
		private const TICK:Number = 200;
		private var _movingAlgorithm:Function;
		public var attached:Boolean = false;
		
		public function Bullet() {
			super();
			_ttl = 1000;
			collidable = true;
		}
		
		override public function update():void 
		{
			super.update();
			if (_damagePerSecond) {
				for(var key:String in _damagedObjects) {
					_damagedObjects[key] -= FlxG.elapsed*1000;
					if (_damagedObjects[key] < 0) delete _damagedObjects[key];
				}
			}
			
			if (_ttl) {
				
				_ttl -= FlxG.elapsed * 1000;
				
				if (_ttl <= 0) {
					_ttl = 1000;
					
					kill();
				}
			}
			
			if (Boolean(_movingAlgorithm))_movingAlgorithm(this);
		}
		
		public function set enemy(val:Boolean):void {
			if (val == _enemy ) return;
			
			
			_enemy = val;
		}
		
		override public function doHurt(obj:Entity):Entity 
		{
			if (obj is Bullet)
				return this;
			if (damagePerTick) { 
				if (isObjectDamaged(obj)) return this;
				else {
					obj.haveHurt(this as Entity, damagePerTick);
					objectDamaged(obj);
				}
			} else {
				obj.haveHurt(this as Entity, damage);
				kill();
			}
			return this;
		}
		override public function haveHurt(obj:Entity, damage:int = 0):Boolean 
		{
			if (!damagePerTick) {
				kill();
				return true;
			}
			return false;
		}
		
		public function set damage(value:Number):void {
			_damagePerSecond = 0;
			_damage = value;
		}
		public function get damage():Number { return _damage; }
		
		
		public function set damagePerSecond(value:Number):void {
			_damagePerSecond = value;
			_damagedObjects = new Object();
		}
		public function get damagePerTick():Number { return _damagePerSecond / 1000 * TICK; }
		
		public function set ttl(value:Number):void { _ttl = value; }
		
		public function set movingAlgorythm(value:Function):void { _movingAlgorithm = value; }
		
		public function isObjectDamaged(object:FlxSprite):Boolean { return Boolean(_damagedObjects[object]); }
		public function objectDamaged(object:FlxSprite):void { _damagedObjects[object] = TICK; }
		
		public static function fire(graphic:Class, x:Number, y:Number, angle:Number, speed:Number, damage:Number, damagePerSecond:Boolean, ttl:Number, movingAlgorithm:Function):Bullet {
			var bullet:Bullet = Registry.$.getFreeObject(Bullet,"attached") as Bullet;
			
			bullet.spawn(x, y);
			bullet.attached = true;
			
			bullet.moveAngle(speed, angle);
			bullet.damage = damage;
			
			if (Boolean(graphic)) bullet.loadGraphic(graphic);
			else bullet.makeGraphic(2, 2, 0xFF000000);
			
			bullet.angle = angle;
			
			if(damagePerSecond)bullet.damagePerSecond = damage;
			bullet.ttl = ttl;
			
			if (Boolean(movingAlgorithm)) bullet.movingAlgorythm = movingAlgorithm;
			
			return bullet;
		}
	}

}
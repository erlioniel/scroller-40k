package guns 
{
	/**
	 * ...
	 * @author Dryga Dmitry
	 */
	public class Flower extends Gun 
	{
		[Embed(source = "../data/bullet_fire_blue.png")] protected var ImgBulletSimple:Class;
		
		public function Flower(object:Unit) 
		{
			super(object);
			
			_damage = 1;
			fireRate = 750;
			speed = 150;
			_bulletTTL = 2000;
			_multishot = 9;
			
			var arr:Array = new Array();
			var i:Number = 9;
			while (i--) {
				arr.push(i*40);
			}
			_angles = arr;
			
			_aimable = false;
			
			_bulletGraphic = ImgBulletSimple;
		}
	}
}
package guns {
	/**
	 * ...
	 * @author Dmitry Dryga
	 */
	
	import flash.utils.getTimer;
	import org.flixel.FlxBasic;
import org.flixel.FlxGroup;
import org.flixel.FlxObject;
	import org.flixel.FlxG;
	import org.flixel.FlxPoint;
	import entities.Player;
	import org.flixel.FlxU;
	
	import plugins.Registry;
	 
	public class Gun extends FlxGroup
	{
        /**
         * Время перезарядки в миллисекундах
         */
		public var fireRate:Number;
		private var _lastShot:Number;

        /**
         * Угол отклонения пушки от направления юнита
         */
		public var angle:Number;

        /**
         * Скорость выпускаемых пуль
         */
		public var speed:Number;
		private var _acceleration:Number;

        /**
         * Угол разброса пуль
         */
		public var angleRandom:Number;

        /**
         * Случайный элемент скорости
         */
        protected var _speedRandom:Number;
		private var _accelerationRandom:Number;
		
		protected var _multishot:Number;

        /**
         * Случайный элемент количества выпускаемых единовременно пуль
         */
		private var _multishotRandom:Number;

        /**
         * Массив углов, под которыми вылетают пули при мультивыстреле
         */
		protected var _angles:Array;

        /**
         * Функция, вызываемая по обновлению пулек пушки.
         */
		public var bulletMoveAlgorithm:Function;

        /**
         * Время жизни пули в миллисекундах
         */
		protected var _bulletTTL:Number;

        /**
         * Случайный элемент времени жизни пули
         */
		protected var _bulletTTLRandom:Number;

        /**
         * Единовременный урон, наносимый пулей
         */
		protected var _damage:Number;

        /**
         * Случайная составляющая урона
         */
		protected var _damageRandom:Number;

        /**
         * Урон, наносимый пулей в секунду. Пуля с этим параметром пролетает сквозь врагов.
         */
		protected var _damagePerSecond:Number;
		
		private var _parent:FlxObject;

        /**
         * Класс графики пули
         */
		protected var _bulletGraphic:Class;

        /**
         * Возможность наведения
         */
		protected var _aimable:Boolean;
		
		public function setDamage(dmg:int, per_second:Boolean = false):void {
			_damage = 0;
			_damagePerSecond = 0;
			if (per_second)
				_damagePerSecond = dmg;
			else
				_damage = dmg;
		}
		
		/**
		 * Создание новой пушки
		 * @param	object Юнит хозяин пушки
		 */
		
		public function Gun(object:Unit) {
			_parent = object;
			
			fireRate = 200;
			_lastShot = 0;
			
			_damage = 0;
			_damageRandom = 0;
			
			speed = 200;
			_speedRandom = 0;
			
			angle = 0;
			angleRandom = 0;
			_angles = new Array();
			
			_bulletTTL = 0;
			_bulletTTLRandom = 0;
			
			_multishot = 0;
			
			_aimable = true;
			
			_offset = new FlxPoint();
		}
		
		
		/**
		 * Пальнуть из пушки
		 */
		public function fire(customAngle:Number = NaN):void {
			if (getTimer() > fireRate + _lastShot )
				_lastShot = getTimer();
			else return;
			
			var multishot:Number = _multishot ? _multishot : 1;
			var i:Number = 0;
			
			while (multishot--) {
				var angle:Number = 0;
				if (!isNaN(customAngle)&&_aimable) angle = customAngle+ random(angleRandom);
				else angle = _parent.angle + (Boolean(_angles[i])?_angles[i]:angle) + random(angleRandom);
				
				var curSpeed:Number = speed + random(_speedRandom);
				
				var damage:Number = _damage + random(_damageRandom);
				
				add(Bullet.fire(_bulletGraphic, x, y, angle, curSpeed, _damagePerSecond?_damagePerSecond:damage, Boolean(_damagePerSecond), _bulletTTL ? (_bulletTTL + random(_bulletTTLRandom)) : 1000, bulletMoveAlgorithm));
				
				i++;
			}
		}
		
		/**
		 * Пальнуть в игрока
		 */
		public function shootObject(obj:FlxObject):void {
            if (!obj || !obj.alive){
                return;
            }
			var angle:Number = FlxU.getAngle(new FlxPoint(x, y), new FlxPoint(obj.x, obj.y)) + random(angleRandom);
			fire(angle);
		}
		
		/**
		 * 
		 */
		public function shootMouse():void {
			var angle:Number = FlxU.getAngle(new FlxPoint(x, y), new FlxPoint(FlxG.mouse.x, FlxG.mouse.y));
			fire(angle);
		}

		internal function get x():Number { return _parent.x+_offset.x*Math.cos(_parent.angle/180*Math.PI)-_offset.y*Math.sin(_parent.angle/180*Math.PI); }
		internal function get y():Number { return _parent.y-_offset.y*Math.cos(_parent.angle/180*Math.PI)-_offset.x*Math.sin(_parent.angle/180*Math.PI); }
		
		private var _offset:FlxPoint;
		public function get offset():FlxPoint { return _offset; }
		
		private static function random(value:Number):Number { return Math.round((Math.random() - 0.5) * value); }
		
		override public function destroy():void 
		{
			for (var k:* in members) {
				if (members[k] is Bullet)
					(members[k] as Bullet).attached = false;
					remove(members[k]);
			}
			super.destroy();
		}
	}
}
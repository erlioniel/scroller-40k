package adapters 
{
	import org.flixel.*;
	import org.flixel.interfaces.FlxGroupInterface;
	/**
	 * Специальный адаптер-прокси для FlxSprite который позволяет использовать
	 * спрайт как FlxGroup. Такая вот магия
	 * 
	 * @author Vladimir Kryukov
	 */
	public class FlxSpriteLayer extends FlxSprite implements FlxGroupInterface
	{	
		private var __private_group:FlxGroup;
		private function get __group():FlxGroup
			{ if (!__private_group) { __private_group = new FlxGroup(); } return __private_group; }
		
		public function FlxSpriteLayer(X:Number = 0, Y:Number = 0, SimpleGraphic:Class = null)
			{ super(X, Y, SimpleGraphic); }
		public function get members():Array
			{ return __group.members; }
		public function set members(value:Array):void
			{ __group.members = value; }
		public function get length():Number
			{ return __group.length; }
		public function set length(value:Number):void
			{ __group.length = value; }
		public function get maxSize():uint
			{ return __group.maxSize; }
		public function set maxSize(size:uint):void
			{ __group.maxSize = size; }
		
		override public function destroy():void
			{ super.destroy(); if(__private_group) { __private_group.destroy(); __private_group = null; } }
		override public function preUpdate():void
			{ super.preUpdate(); if(__private_group) { __private_group.preUpdate(); } }
		override public function update():void
			{ super.update(); if(__private_group) { __private_group.update(); } }
		override public function draw():void
			{ if (__private_group) { __private_group.draw(); } super.draw(); }
		override public function kill():void
			{ super.kill(); if (__private_group) { __private_group.kill(); } }
		
		public function getMembers():Array
			{ return __group.members; }
		public function add(Object:FlxBasic):FlxBasic
			{ return __group.add(Object) }
		public function recycle(ObjectClass:Class = null):FlxBasic
			{ return __group.recycle(ObjectClass); }
		public function remove(Object:FlxBasic, Splice:Boolean = false):FlxBasic
			{ return __group.remove(Object,Splice); }
		public function replace(OldObject:FlxBasic, NewObject:FlxBasic):FlxBasic
			{ return __group.replace(OldObject,NewObject); }
		
	}

}
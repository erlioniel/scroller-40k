package adapters 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.flixel.*;
	import adapters.FlxTileblockOverflow;
	
	public class FlxTileblockOverflowMasked extends FlxTileblockOverflow
	{
		
		protected var _gridsize:uint;
		protected var _mask:Array;
		
		public function FlxTileblockOverflowMasked(X:int,Y:int,Width:uint,Height:uint, mask:Array, gridsize:uint = 1)
		{
			super(X, Y, Width, Height);
			
			_mask = mask;
			_gridsize = gridsize;
		}
		
		override public function loadTiles(TileGraphic:Class, TileWidth:uint = 0, TileHeight:uint = 0, Empties:uint = 0):FlxTileblock 
		{
			// Call original function
			super.loadTiles(TileGraphic, TileWidth, TileHeight, Empties);
			
			var row:uint = 0;
			var column:uint;
			var widthInTiles:uint = width/_gridsize;
			var heightInTiles:uint = height / _gridsize;
			
			var tempSprite:FlxSprite = new FlxSprite().makeGraphic(width, height,0);
			tempSprite.stamp(this);
			var bitmapData:BitmapData = tempSprite.framePixels;
			
			_pixels = new BitmapData(width,height,true,0x00ff0000);
			
			while(row < heightInTiles)
			{
				column = 0;
				if (_mask[row] && _mask[row] is Array) { while(column < widthInTiles)
				{
					if (_mask[row][column]) {
						_pixels.copyPixels(bitmapData, new Rectangle(column * _gridsize, row * _gridsize, _gridsize, _gridsize), new Point(column * _gridsize, row * _gridsize), null, null, true);
					}
					column++;
				} }
				row++;
			}
			resetHelpers();
			calcFrame();
			bitmapData = null;
			return this;
		}
		
		protected function stampRect(Brush:FlxSprite, x:uint = 0, y:uint = 0, w:uint = 1, h:uint = 1):void {
			
		}
		
	}

}
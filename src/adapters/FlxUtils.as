package  adapters
{
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class FlxUtils 
	{
		
		public function FlxUtils() { }
		
		/**
		 * Adding zeros in start or end of string
		 * 
		 * @param	to			Original string
		 * @param	lenght		Needed lenght
		 * @param	toEnd		If false - zeros added in front of string
		 * @return				Result string
		 */
		public static function addZero(to:String, lenght:uint, toEnd:Boolean = false):String {
			while (to.length < lenght) {
				if (toEnd) to += "0";
				else to = "0" + to;
			}
			return to;
		}
		
		/**
		 * Return FlxRect of object
		 * 
		 * @param	obj		Object to convert
		 * @return			FlxRect
		 */
		public static function getRect(obj:Object):FlxRect
		{
			return new FlxRect(obj.x,obj.y,obj.width,obj.height);
		}
		
		public static function arrayRandom(array:Array):* {
			return array[Math.floor(FlxG.random() * (array.length))];
		}
		
	}

}
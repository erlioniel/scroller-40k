package  adapters
{
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import org.flixel.plugin.photonstorm.BaseTypes.Bullet;
	import flash.utils.getTimer;
	
	public class FlxWeaponDelta extends FlxWeapon
	{
		public var damage:Number;
		
		public function FlxWeaponDelta(name:String, parentRef:* = null, xVariable:String = "x", yVariable:String = "y") {
			super(name, parentRef, xVariable, yVariable);
			
			bounds = new FlxRect(0, 0, Level.width*Level.gridSize, Level.height*Level.gridSize);
			
			damage = 2;
		}
		
		override protected function runFire(method:uint, x:int = 0, y:int = 0, target:FlxSprite = null, angle:int = 0):Boolean
		{
			if (fireRate > 0 && (getTimer() < nextFire))
			{
				return false;
			}
			
			var bullets:Array = new Array();
			var numOfBullets:uint;
			
			if (multiShot) numOfBullets = multiShot;
			else numOfBullets = 1;
			
			while(numOfBullets--) {
				var bullet:Bullet = getFreeBullet();
				bullets.push(bullet);
				if (bullet == null) return false;
			}

			if (onPreFireCallback is Function)
			{
				onPreFireCallback.apply();
			}
			
			if (onPreFireSound)
			{
				onPreFireSound.play();
			}
			
			for each (var tempBullet:Bullet in bullets){			
				currentBullet = tempBullet;
				
				//	Clear any velocity that may have been previously set from the pool
				currentBullet.velocity.x = 0;
				currentBullet.velocity.y = 0;
				
				lastFired = getTimer();
				nextFire = getTimer() + fireRate;
				
				var launchX:int = positionOffset.x;
				var launchY:int = positionOffset.y;
				
				if (fireFromParent)
				{
					launchX += parent[parentXVariable];
					launchY += parent[parentYVariable];
				}
				else if (fireFromPosition)
				{
					launchX += fireX;
					launchY += fireY;
				}
				
				if (directionFromParent)
				{
					velocity = FlxVelocity.velocityFromFacing(parent, bulletSpeed);
				}
				
				//	Faster (less CPU) to use this small if-else ladder than a switch statement
				if (method == FIRE)
				{
					currentBullet.fire(launchX, launchY, velocity.x, velocity.y);
				}
				else if (method == FIRE_AT_MOUSE)
				{
					currentBullet.fireAtMouse(launchX, launchY, bulletSpeed);
				}
				else if (method == FIRE_AT_POSITION)
				{
					currentBullet.fireAtPosition(launchX, launchY, x, y, bulletSpeed);
				}
				else if (method == FIRE_AT_TARGET)
				{
					currentBullet.fireAtTarget(launchX, launchY, target, bulletSpeed);
				}
				else if (method == FIRE_FROM_ANGLE)
				{
					currentBullet.fireFromAngle(launchX, launchY, angle, bulletSpeed);
				}
				else if (method == FIRE_FROM_PARENT_ANGLE)
				{
					currentBullet.fireFromAngle(launchX, launchY, parent.angle, bulletSpeed);
				}
				
				bulletsFired++;
			}
			
			if (onPostFireCallback is Function)
			{
				onPostFireCallback.apply();
			}
			
			if (onPostFireSound)
			{
				onPostFireSound.play();
			}
			
			return true;
		}
		override protected function getFreeBullet():Bullet
		{
			var result:Bullet = null;
			
			if (group == null || group.length == 0)
			{
				throw new Error("Weapon.as cannot fire a bullet until one has been created via a call to makePixelBullet or makeImageBullet");
				return null;
			}
			
			for each (var bullet:Bullet in group.members)
			{
				if (bullet.exists == false)
				{
					result = bullet;
					bullet.exists = true;
					break;
				}
			}
			
			return result;
		}
		override public function setFiringPosition(x:int, y:int, offsetX:int = NaN, offsetY:int = NaN):void 
		{
			fireFromPosition = true;
			fireX = x;
			fireY = y;
			
			if( isNaN(offsetX))positionOffset.x = offsetX;
			if( isNaN(offsetY))positionOffset.y = offsetY;
		}
	}

}
package adapters 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.flixel.*;
	
	public class FlxTileblockOverflow extends FlxTileblock
	{
		
		static public const HIDDEN:uint = 1;
		static public const DRAW:uint = 0;
		
		public var overflow:int = HIDDEN;
		
		public function FlxTileblockOverflow(X:int,Y:int,Width:uint,Height:uint)
		{
			super(X, Y, Width, Height);
		}
		
		override public function loadTiles(TileGraphic:Class, TileWidth:uint = 0, TileHeight:uint = 0, Empties:uint = 0):FlxTileblock 
		{
			// Save original size
			var originalWidth:int = width;
			var originalHeight:int = height;
			
			// Call original function
			super.loadTiles(TileGraphic, TileWidth, TileHeight, Empties);
			
			// Check overflow
			if (overflow == HIDDEN) {
				if(width != originalWidth || height != originalHeight) {
					var tempSprite:FlxSprite = new FlxSprite().makeGraphic(width, height,0x00000000);
					tempSprite.stamp(this);
					width = originalWidth;
					height = originalHeight;
					makeGraphic(originalWidth, originalHeight, 0, true);
					stamp(tempSprite, 0, 0);
					tempSprite = null;
				}
			}
			
			return this;
		}
		
	}

}
package adapters 
{
	/**
	 * FlxNiceButton - based on FlxButtonPlus
	 * 
	 * @version 1.0
	 * @link http://pixelgame.ru
	 * @author Vladimir Kryukov
	**/
	
	import flash.events.MouseEvent;
	
	import plugins.*;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class FlxNiceButton extends FlxGroup
	{
		
		static public var NORMAL:uint = 0;
		static public var HIGHLIGHT:uint = 1;
		static public var PRESSED:uint = 2;
		static public var UPDATE:uint = 3;
		
		[Embed(source = "../data/sound/click.mp3")] protected var SoundClick:Class;
		protected var _sound:FlxSound;
		
		/**
		 * Set this to true if you want this button to function even while the game is paused.
		 */
		public var pauseProof:Boolean;
		/**
		 * Shows the current state of the button.
		 */
		protected var _status:uint;
		/**
		 * This function is called when the button is clicked.
		 */
		protected var _onClick:Function;
		/**
		 * Tracks whether or not the button is currently pressed.
		 */
		protected var _pressed:Boolean;
		/**
		 * Whether or not the button has initialized itself yet.
		 */
		protected var _initialized:Boolean;
		
		//	Flixel Power Tools Modification from here down
		
		public var buttonSprite:FlxSprite;
		public var textNormal:FlxText;
		public var textHighlight:FlxText;
		
		/**
		 * The parameters passed to the _onClick function when the button is clicked
		 */
		private var onClickParams:Array;
		
		/**
		 * This function is called when the button is hovered over
		 */
		private var enterCallback:Function;
		
		/**
		 * The parameters passed to the enterCallback function when the button is hovered over
		 */
		private var enterCallbackParams:Array;
		
		/**
		 * This function is called when the mouse leaves a hovered button (but didn't click)
		 */
		private var leaveCallback:Function;
		
		/**
		 * The parameters passed to the leaveCallback function when the hovered button is left
		 */
		private var leaveCallbackParams:Array;
		
		private var _x:int;
		private var _y:int;
		public var width:int;
		public var height:int;
		
		protected var _stateCurrent:String;
		protected var _stateLib:Array;
		
		public var y_offset:int;
		
		/**
		 * Creates a new <code>FlxButton</code> object with a gray background
		 * and a callback function on the UI thread.
		 * 
		 * @param	X			The X position of the button.
		 * @param	Y			The Y position of the button.
		 * @param	Callback	The function to call whenever the button is clicked.
		 * @param	Params		An optional array of parameters that will be passed to the Callback function
		 * @param	Label		Text to display on the button
		 * @param	Width		The width of the button.
		 * @param	Height		The height of the button.
		 */
		public function FlxNiceButton(X:int, Y:int, Callback:Function, Params:Array = null, Label:String = null, Width:int = 100, Height:int = 20):void
		{
			super(3);
			
			_stateCurrent = "default";
			_stateLib = new Array();
			
			_x = X;
			_y = Y;
			y_offset = -6;
			width = Width;
			height = Height;
			_onClick = Callback;
			
			buttonSprite = new FlxSprite(X, Y);
			buttonSprite.makeGraphic(Width, Height, 0xff555555);
			buttonSprite.solid = false;
			buttonSprite.scrollFactor.x = 0;
			buttonSprite.scrollFactor.y = 0;
			
			add(buttonSprite);
			
			if (Label != null)
			{
				textNormal = new FlxText(X, Y + (height/2 + y_offset), Width, Label);
				textNormal.setFormat(null, 8, 0xffffffff, "center", 0xff000000);
				textNormal.solid = false;
				textNormal.scrollFactor.x = 0;
				textNormal.scrollFactor.y = 0;
				
				textHighlight = new FlxText(X, Y + (height/2 + y_offset), Width, Label);
				textHighlight.setFormat(null, 8, 0xffffffff, "center", 0xff000000);
				textHighlight.solid = false;
				textHighlight.scrollFactor.x = 0;
				textHighlight.scrollFactor.y = 0;
				
				add(textNormal);
				add(textHighlight);
			}

			_status = NORMAL;
			_pressed = false;
			_initialized = false;
			pauseProof = true;
			
			_sound = Registry.$.getFreeObject(FlxSound);
			_sound.loadEmbedded(SoundClick);
			
			onClickParams = new Array();
			if (Params) {
				onClickParams = Params;
			}
			onClickParams.unshift(this);
		}
		
		public function set x(newX:int):void
		{
			_x = newX;
			
			buttonSprite.x = _x;
			
			if (textNormal)
			{
				textNormal.x = _x;
				textHighlight.x = _x;
			}
		}
		
		public function get x():int
		{
			return _x;
		}
		
		public function set y(newY:int):void
		{
			_y = newY;
			
			buttonSprite.y = _y;
			
			if (textNormal)
			{
				textNormal.y = _y  + (height/2 + y_offset);
				textHighlight.y = _y + (height/2 + y_offset);
			}
		}
		
		public function get y():int
		{
			return _y;
		}
		
		public function stateAdd(Name:String, frameRate:Number = 1, loop:Boolean = true, animNormal:Array = null, animHover:Array = null, animPressed:Array = null):void {
			_stateLib.push(Name);
			if (animNormal && animNormal.length > 0) buttonSprite.addAnimation(Name+"_idle", animNormal, frameRate, loop);
			if (animHover && animHover.length > 0) buttonSprite.addAnimation(Name+"_hover", animHover, frameRate, loop);
			if (animPressed && animPressed.length > 0) buttonSprite.addAnimation(Name+"_pressed", animPressed, frameRate, loop);
		}
		
		public function stateSelect(Name:String = "default"):void {
			if (stateExists(Name)) {
				_stateCurrent = Name;
				_status = UPDATE;
			}
		}
		
		public function stateExists(Name:String):Boolean {
			var n:Number = _stateLib.length;
			for (var i:int = 0; i < n; i++) {
				if (Name == _stateLib[i]) return true;
			}
			return false;
		}
		public function get stateCurrent():String {
			return _stateCurrent;
		}
		
		override public function preUpdate():void
		{
			super.preUpdate();
			
			if (!_initialized)
			{
				if(FlxG.stage != null)
				{
					FlxG.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
					_initialized = true;
				}
			}
		}
		
		public function loadGraphic(graphicClass:Class, frameRate:Number = 1, loop:Boolean = true, animNormal:Array = null, animHover:Array = null, animPressed:Array = null):void
		{
			remove(buttonSprite);
			
			buttonSprite = new FlxSprite(x, y);
			buttonSprite.loadGraphic(graphicClass, true, false, width, height);
			
			stateAdd("default", frameRate, loop, animNormal, animHover, animPressed);
			stateSelect("default");
			
			buttonSprite.visible = true;
			buttonSprite.solid = false;
			buttonSprite.scrollFactor.x = 0;
			buttonSprite.scrollFactor.y = 0;
			
			add(buttonSprite);
		}
		
		public function makeGraphic(width:uint, height:uint, color:uint):void {
			remove(buttonSprite);
			
			buttonSprite = new FlxSprite(x, y);
			buttonSprite.makeGraphic(width, height, color);
			
			stateAdd("default");
			stateSelect("default");
			
			buttonSprite.visible = true;
			buttonSprite.solid = false;
			buttonSprite.scrollFactor.x = 0;
			buttonSprite.scrollFactor.y = 0;
			
			add(buttonSprite);
		}
		
		/**
		 * Called by the game loop automatically, handles mouseover and click detection.
		 */
		override public function update():void
		{
			updateButton(); //Basic button logic
		}
		
		/**
		 * Basic button update logic
		 */
		protected function updateButton():void
		{
			var prevStatus:uint = _status;
			
			if (FlxG.mouse.visible)
			{
				if (buttonSprite.cameras == null) buttonSprite.cameras = FlxG.cameras;
				
				var c:FlxCamera;
				var i:uint = 0;
				var l:uint = buttonSprite.cameras.length;
				var offAll:Boolean = true;
				
				while(i < l)
				{
					c = buttonSprite.cameras[i++] as FlxCamera;
					
					if (FlxMath.mouseInFlxRect(false,FlxUtils.getRect(buttonSprite)))
					{
						offAll = false;
						
						if (FlxG.mouse.justPressed()) _status = PRESSED;
						if (FlxG.mouse.justReleased()) _status = HIGHLIGHT;
						if (_status == NORMAL) _status = HIGHLIGHT;
					}
				}
				
				if (offAll)
				{
					_status = NORMAL;
				}
			}
			
			if (_status != prevStatus)
			{
				if (_status == NORMAL)
				{
					play(_stateCurrent+"_idle");
					if (textNormal) {
						textNormal.visible = true;
						textHighlight.visible = false;
					}
					
					if (leaveCallback is Function) leaveCallback.apply(null, leaveCallbackParams);
				}
				else if (_status == HIGHLIGHT)
				{	
					play(_stateCurrent+"_hover");
					if (textNormal) {
						textNormal.visible = false;
						textHighlight.visible = true;
					}
					
					if (enterCallback is Function) enterCallback.apply(null, enterCallbackParams);
				}
				else if (_status == PRESSED) {
					_sound.play();
					_sound.alive = true;
					play(_stateCurrent+"_pressed",true);
				}
			}
		}
		
		/**
		 * Called by the game state when state is changed (if this object belongs to the state)
		 */
		override public function destroy():void
		{
			if (FlxG.stage != null)
			{
				FlxG.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			}
			
			if (buttonSprite != null)
			{
				buttonSprite.destroy();
				buttonSprite = null;
			}
			
			if (textNormal != null)
			{
				textNormal.destroy();
				textNormal = null;
			}
			
			if (textHighlight != null)
			{
				textHighlight.destroy();
				textHighlight = null;
			}
			
			_onClick = null;
			for (var k:* in onClickParams) {
				onClickParams[k] = null;
			}
			enterCallback = null;
			leaveCallback = null;
			_stateLib = null;
			
			_sound.alive = false;
			_sound = null;
			
			super.destroy();
		}
		
		/**
		 * Internal function for handling the actual callback call (for UI thread dependent calls like <code>FlxU.openURL()</code>).
		 */
		protected function onMouseUp(event:MouseEvent):void
		{
			if (exists && visible && active && (_status == PRESSED) && (_onClick != null) && (pauseProof || !FlxG.paused))
			{
				_onClick.apply(null, onClickParams);
			}
		}
		
		/**
		 * If this button has text, set this to change the value
		 */
		public function set text(value:String):void
		{
			if (textNormal && textNormal.text != value)
			{
				textNormal.text = value;
				textHighlight.text = value;
			}
		}
		
		/**
		 * Center this button (on the X axis) Uses FlxG.width / 2 - button width / 2 to achieve this.<br />
		 * Doesn't take into consideration scrolling
		 */
		public function screenCenter():void
		{
			buttonSprite.x = (FlxG.width / 2) - (width / 2);
			
			if (textNormal)
			{
				textNormal.x = buttonSprite.x;
				textHighlight.x = buttonSprite.x;
			}
		}
		
		/**
		 * Sets a callback function for when this button is rolled-over with the mouse
		 * 
		 * @param	callback	The function to call, will be called once when the mouse enters
		 * @param	params		An optional array of parameters to pass to the function
		 */
		public function setMouseOverCallback(callback:Function, params:Array = null):void
		{
			enterCallback = callback;
			
			enterCallbackParams = params;
		}
		
		/**
		 * Sets a callback function for when the mouse rolls-out of this button
		 * 
		 * @param	callback	The function to call, will be called once when the mouse leaves the button
		 * @param	params		An optional array of parameters to pass to the function
		 */
		public function setMouseOutCallback(callback:Function, params:Array = null):void
		{
			leaveCallback = callback;
			
			leaveCallbackParams = params;
		}
		
		/**
		 * Sets a callback function for when the mouse clicks on this button
		 * 
		 * @param	callback	The function to call whenever the button is clicked.
		 * @param	params		An optional array of parameters that will be passed to the Callback function
		 */
		public function setOnClickCallback(callback:Function, params:Array = null):void
		{
			_onClick = callback;
			
			if (params)
			{
				onClickParams = params;
			}
			
			onClickParams.unshift(this);
		}
		/**
		 * Play animation on button
		 * 
		 * @param	Anim		The animation title
		 * @param	Force		Force restart animation
		 */
		public function play(Anim:String,Force:Boolean = false):void {
			buttonSprite.playIfExists(Anim, Force);
		}
		
	}

}
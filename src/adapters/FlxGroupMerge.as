package adapters 
{
	import org.flixel.*;
	
	public class FlxGroupMerge extends FlxGroup
	{
		
		public function FlxGroupMerge(maxSize:uint = 0) 
		{
			super(maxSize);
		}
		
		/**
		 * Function merge all sprites in group and return it
		 * 
		 * @return Result sprite
		 */
		public function merge():FlxSprite {
			// Координаты слепленного спрайта
			var tempX:Number;
			var tempY:Number;
			var tempW:uint;
			var tempH:uint;
			var n:uint = 0;
			// Перебираем в поисках самых будущего размера спрайта и его координат
			while (n < members.length) {
				if (!(members[n] is FlxSprite)) { n++; continue; }
				if(!members[n].visible) { n++; continue; }
				//var current:FlxSprite =  as FlxSprite;
				// Инициализация
				if (tempW == 0) {
					tempX = members[n].x;
					tempY = members[n].y;
					tempW = members[n].width;
					tempH = members[n].height;
				}
				// Расширение блока при необходимости
				else {
					if (members[n].x < tempX) {
						tempW += (tempX - members[n].x);
						tempX = members[n].x;
					}
					if (members[n].y < tempY) {
						tempH += (tempY - members[n].y);
						tempY = members[n].y;
					}
					if (members[n].x + members[n].width > tempX + tempW) {
						tempW += members[n].x + members[n].width - (tempX + tempW);
					}
					if (members[n].y + members[n].height > tempY + tempH) {
						tempH += members[n].y + members[n].height - (tempY + tempH);
					}
				}
				n++;
			}
			var resultSprite:FlxSprite = new FlxSprite(tempX, tempY).makeGraphic(tempW, tempH, 0x00000000);
			n = 0;
			while (n < members.length) {
				if (!(members[n] is FlxSprite)) { n++; continue; }
				if(!members[n].visible) { n++; continue; }
				// Перерисовываем
				resultSprite.stamp(members[n], members[n].x - resultSprite.x, members[n].y - resultSprite.y);
				n++;
			}
			return resultSprite;
		}
		
	}

}
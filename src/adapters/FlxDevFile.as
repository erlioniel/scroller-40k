package adapters 
{
	import org.flixel.FlxG;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.FileReference;
	import flash.net.FileFilter;
	import flash.utils.ByteArray;
	
	public class FlxDevFile extends FileReference
	{
		static protected const FILE_TYPES:Array = [new FileFilter("Pixelgame Data File", "*.dat")];
		static protected const DEFAULT_FILE_NAME:String = "data.dat";
		
		protected static var _file:FileReference;
		protected static var _callback:Function;
		
		public static function load(callback:Function = null):void {
			if (callback == null) return;
			_callback = callback;
			_file = new FileReference();
			_file.addEventListener(Event.SELECT, onOpenSelect);
			_file.addEventListener(Event.CANCEL, onCancel);
			_file.browse(FILE_TYPES);
		}
		
		public static function save(data:String, filename:String = null):void {
			if (!data || data.length == 0) return;
			_file = new FileReference();
			_file.addEventListener(Event.COMPLETE, onSaveComplete);
			_file.addEventListener(Event.CANCEL,onCancel);
			_file.addEventListener(IOErrorEvent.IO_ERROR, onError);
			_file.save(data, (filename ? filename : DEFAULT_FILE_NAME));
		}
		
		public static function onSaveComplete(E:Event=null):void {
			FlxG.log("DEVFILE: successfully saved file");
			kill();
		}
		
		public static function onOpenSelect(E:Event=null):void {
			_file.addEventListener(Event.COMPLETE, onOpenComplete);
			_file.addEventListener(IOErrorEvent.IO_ERROR, onError);
			_file.load();
		}
		
		public static function onOpenComplete(E:Event=null):void {
			//Turn the file into a giant string
			var fileContents:String = null;
			var data:ByteArray = _file.data;
			if(data != null)
				fileContents = data.readUTFBytes(data.bytesAvailable);
			_file = null;
			if((fileContents == null) || (fileContents.length <= 0))
			{
				FlxG.log("ERROR: Empty file");
				return;
			}
			
			_callback.apply(null, [fileContents]);
		}
		
		public static function onCancel(E:Event=null):void {
			kill();
		}
		
		public static function onError(E:Event=null):void {
			FlxG.log("ERROR: problem load or save file");
			kill();
		}
		
		public static function kill():void {
			_file.removeEventListener(Event.SELECT, onOpenSelect);
			_file.removeEventListener(Event.CANCEL, onCancel);
			_file.removeEventListener(Event.COMPLETE,onSaveComplete);
			_file.removeEventListener(Event.COMPLETE,onOpenComplete);
			_file.removeEventListener(Event.CANCEL,onCancel);
			_file.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			_file = null;
		}
		
	}

}
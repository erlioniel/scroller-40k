package adapters 
{
	import org.flixel.FlxText;
	import org.flixel.FlxG;
	import org.flixel.plugin.photonstorm.*;
	
	public class FlxDevSprite extends FlxExtendedSprite
	{
		public static var _blocks:Array;
		public static var _blocksSel:uint = 0;
		
		public var _coord:FlxText;
		public var isSelected:Boolean = false;
		public var isLocked:Boolean = false;
		
		public function FlxDevSprite(X:Number = 0, Y:Number = 0, SimpleGraphic:Class = null)
		{
			if (FlxG.getPlugin(FlxMouseControl) == null)
				{ FlxG.addPlugin(new FlxMouseControl); }
			
			super(X, Y, null);
			
			width = Level.gridSize;
			height = Level.gridSize;
			makeGraphic(Level.gridSize, Level.gridSize, 0xffff0000, true);
			alpha = 0.5;
			visible = true;
			immovable = true;
			
			_coord = new FlxText(0, 0, 100);
			_coord.setFormat(null, Level.gridSize, 0xffff0000, "left", 0xff000000);
			_coord.x = x + 4;
			_coord.y = y + 3;
			_coord.text = Math.floor(x/Level.gridSize)+":"+Math.floor(y/Level.gridSize);
			_coord.visible = true;
			_coord.immovable = true;
			StatePlay._state._hud.add(_coord);
			
			mousePressedCallback = onClick;
			enableMouseClicks(false);
			
			if (!_blocks) _blocks = new Array();
			_blocks.push(this);
		}
		
		public function onClick(Obj:*, cX:int, cY:int):void {
			if (FlxG.keys.pressed("SHIFT")) select();
		}
		
		override public function destroy():void 
		{
			super.destroy();
			_coord = null;
		}
		
		override public function kill():void 
		{
			super.kill();
			if (isSelected) select();
			if (_coord) _coord.kill();
			isLocked = true;
			destroy();
		}
		
		public function redraw(nX:int, nY:int):void {
			if (nX <= x+Level.gridSize && nX >= x && nY <= y+Level.gridSize && nY >= y) return;
			
			if (nX > x) width = nX - x;
			else { width = x - nX + 2*Level.gridSize; x = nX -Level.gridSize; }
			
			if (nY > y) height = nY - y;
			else { height = y - nY + 2*Level.gridSize; y = nY -Level.gridSize; }
			
			redrawText();
		}
		
		public function redrawText():void {
			if (width > Level.gridSize || height > Level.gridSize) {
				makeGraphic(width, height, 0x66ff0000, true);
				_coord.text = Math.floor(x/Level.gridSize)+":"+Math.floor(y/Level.gridSize)+" S:" + Math.floor(width / Level.gridSize) + ":" + Math.floor(height / Level.gridSize);
				_coord.x = x + 4;
				_coord.y = y + 3;
			} else {
				width = Level.gridSize;
				height = Level.gridSize;
			}
		}
		
		public function resize(nW:uint, nH:uint):void {
			redraw(x + nW, y + nH);
		}
		
		public static function moveSelected(nX:int, nY:int):void {
			var i:int;
			for (i = 0; i < _blocks.length; i++) {
				if (_blocks[i] is FlxDevSprite && _blocks[i].isSelected && !_blocks[i].isLocked) {
					_blocks[i].x += nX;
					_blocks[i].y += nY;
					_blocks[i].redrawText();
				}
			}
		}
		
		public static function selectAll(deselect:Boolean = false):void {
			var i:int;
			for (i = 0; i < _blocks.length; i++) {
				if (_blocks[i] is FlxDevSprite && !_blocks[i].isLocked) {
					if (!deselect && !_blocks[i].isSelected) _blocks[i].select();
					if (deselect && _blocks[i].isSelected) _blocks[i].select();
				}
			}
		}
		
		public static function deleteSelection():void {
			var i:int;
			for (i = 0; i < _blocks.length; i++) {
				if (_blocks[i] is FlxDevSprite && _blocks[i].isSelected && !_blocks[i].isLocked) {
					_blocks[i].kill();
				}
			}
		}
		
		public function select():void {
			if (isLocked) return;
			if (isSelected) {
				isSelected = false;
				_blocksSel -= 1;
				alpha = 0.5;
			}
			else {
				isSelected = true;
				_blocksSel += 1;
				alpha = 0.8;
			}
		}
	}

}
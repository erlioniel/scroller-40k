package adapters 
{
	import org.flixel.*;
	
	public class FlxDevMapeditor extends FlxGroup
	{
		protected var _debug_sprite:FlxDevSprite;
		protected var _debug_coords:FlxPoint;
		
		[Embed(source = "../data/grid.png")] public var ImgDevGrid:Class;
		
		public function FlxDevMapeditor() 
		{
			super();
			
			if (!FlxG.debug) return;
			
			Level.addBlock(0, 0, Level.width, Level.height, ImgDevGrid, false);
			
			visible = true;
			
			StatePlay._state._hud.add(this);
		}
		
		override public function update():void 
		{
			super.update();
			
			FlxG.collide(this, StatePlay._state._objects);
			
			// Если выбраны какие-то блоки
			if (FlxDevSprite._blocksSel > 0) {
				if (FlxG.keys.justPressed("LEFT")) FlxDevSprite.moveSelected( -Level.gridSize*(FlxG.keys.pressed("SHIFT") ? 10 : 1), 0);
				if (FlxG.keys.justPressed("RIGHT")) FlxDevSprite.moveSelected( Level.gridSize*(FlxG.keys.pressed("SHIFT") ? 10 : 1), 0);
				if (FlxG.keys.justPressed("UP")) FlxDevSprite.moveSelected( 0, -Level.gridSize*(FlxG.keys.pressed("SHIFT") ? 10 : 1));
				if (FlxG.keys.justPressed("DOWN")) FlxDevSprite.moveSelected( 0, Level.gridSize*(FlxG.keys.pressed("SHIFT") ? 10 : 1));
				if (FlxG.keys.justPressed("DELETE")) FlxDevSprite.deleteSelection();
			}
			if (FlxG.keys.pressed("CONTROL")) {
				if (FlxG.mouse.justPressed()) {
					_debug_coords = FlxG.mouse.getWorldPosition();
					_debug_sprite = new FlxDevSprite(Math.floor(_debug_coords.x/Level.gridSize) * Level.gridSize,Math.floor(_debug_coords.y/Level.gridSize) * Level.gridSize);
					add(_debug_sprite);
				}
				else if (FlxG.mouse.justReleased()) {
					_debug_coords = FlxG.mouse.getWorldPosition();
					_debug_sprite.redraw(Math.ceil(_debug_coords.x / Level.gridSize) * Level.gridSize, Math.ceil(_debug_coords.y / Level.gridSize)*Level.gridSize);
				}
			}
			if (FlxG.keys.pressed("SHIFT")) {
				if (FlxG.keys.justPressed("A")) FlxDevSprite.selectAll(false);
				if (FlxG.keys.justPressed("D")) FlxDevSprite.selectAll(true);
				if (FlxG.keys.justPressed("X")) FlxDevFile.save(exportBlocks(),"collision.dat");
				if (FlxG.keys.justPressed("C")) FlxDevFile.save(saveBlocks(),"blocks.dat");
				if (FlxG.keys.justPressed("V")) FlxDevFile.load(loadBlocks);
				if (FlxG.keys.justPressed("Z")) _debug_sprite.kill();
			}
		}
		
		protected function deleteAll():void {
			callAll("kill");
		}
		
		// TODO Вынести в приличный класс управление матрицами (в т.ч. вынести их экспорт/импорт, запись и получение значений и так далее)
		protected function matrixCreate(width:uint, height:uint):Array {
			var array:Array = [];
			var l:int;
			var n:int;
			for (l = 0; l < height; l++) {
				array[l] = [];
				for (n = 0; n < width; n++) array[l][n] = 0;
			}
			return array;
		}
		
		protected function matrixAddCol(matrix:Array):Array {
			var l:int;
			for (l = 0; l < matrixRows(matrix); l++ )
				{ matrix[l][matrixCols(matrix,l)] = 0; }
			return matrix;
		}
		
		protected function matrixAddRow(matrix:Array):Array {
			var l:int;
			var c_row:uint = matrixRows(matrix);
			for (l = 0; l < matrixCols(matrix); l++ )
				{ matrix[c_row][l] = 0; }
			return matrix;
		}
		
		protected function matrixCols(matrix:Array, row:uint = 0):uint {
			return matrix[row].lenght;
		}
		
		protected function matrixRows(matrix:Array):uint {
			return matrix.lenght;
		}
		
		protected function exportBlocks():String {
			var array:Array = matrixCreate(Level.width,Level.height);
			var n:int;
			var l:int;
			var i:int;
			// Перебираем блоки, заполняя массив
			for (i = 0; i < members.length; i++) {
				if (!members[i]) continue;
				if (!members[i].alive) continue;
				for (l = 0; l < Math.floor(members[i].height / Level.gridSize); l++) {
					if (l == 0) continue;
					for (n = 0; n < Math.floor(members[i].width / Level.gridSize); n++) {
						if (n == 0) continue;
						FlxG.log("Try to add to "+Math.floor(members[i].y / Level.gridSize)+"+" + l+":"+Math.floor(members[i].x / Level.gridSize)+"+" + n);
						if (matrixRows(array) <= Math.floor(members[i].y / Level.gridSize) + l) array = matrixAddRow(array);
						if (matrixCols(array) <= Math.floor(members[i].x / Level.gridSize) + n) array = matrixAddCol(array);
						array[Math.floor(members[i].y / Level.gridSize) + l][Math.floor(members[i].x / Level.gridSize) + n] = 1;
					}
				}
			}
			// Формируем и возращаем строку
			var s:String = "";
			for (l = 0; l < Level.height; l++) {
				for (n = 0; n < Level.width; n++) {
					if (n != 0) s += ",";
					s += array[l][n];
				}
				s += "\n";
			}
			FlxG.log(s);
			return s;
		}
		
		protected function saveBlocks():String {
			var i:int = 0;
			var res:Array = [];
			var s:String = "";
			for (i = 0; i < members.length; i++) {
				if (!members[i]) continue;
				if (!members[i].alive) continue;
				if (members[i].width < Level.gridSize || members[i].height < Level.gridSize) continue;
				if (s.length > 0) s += "\n";
				s += members[i].x + "," + members[i].y + "," + members[i].width + "," + members[i].height;
			}
			return s;
		}
		
		public function loadBlocks(data:String):void {
			
			deleteAll();
			
			var i:int = 0;
			var lines:Array = data.split("\n");
			var line_data:Array;
			for (i = 0; i < lines.length; i++) {
				line_data = lines[i].split(",");
				_debug_sprite = new FlxDevSprite(line_data[0], line_data[1]);
				_debug_sprite.resize(line_data[2], line_data[3]);
				add(_debug_sprite);
			}
			
		}
		
	}

}
package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Vladimir Kryukov
	 */
	public class Backgound extends FlxGroup
	{
		protected var _scroll_speed:int;
		protected var _scroll_multi:Number;
		
		protected var _current:FlxSprite;
		protected var _next:FlxSprite;
		
		public function Backgound() {
			super();
			exists = false;
			_scroll_speed = 100;
			_scroll_multi = 1.0;
		}
		
		public function start():void {
			_current = (getRandom() as FlxSprite);
			_current.y = 0;
			_current.alive = true;
			exists = true;
			
			random_piece();
			recalculate_speed();
		}
		
		override public function destroy():void 
		{
			super.destroy();
			_current = null;
			_next = null;
		}
		
		override public function update():void {
			if (_current && !_current.onScreen()) {
				_current.alive = false;
				_current = _next;
				random_piece();
			}
			super.update();
		}
		
		override public function add(Object:FlxBasic):FlxBasic 
		{
			if (Object is FlxSprite) {
				(Object as FlxSprite).y = FlxG.height * 2;
				(Object as FlxSprite).alive = false;
			}
			return super.add(Object);
		}
		
		protected function random_piece(i:int = 0):void {
			if (members.length < 2) return;
			if (i > 10) return;
			var obj:FlxBasic = getRandom();
			if (obj is FlxSprite) {
				if (!(obj as FlxSprite).onScreen()) {
					_next = (obj as FlxSprite);
					_next.y = _current.y - _current.height;
					_next.alive = true;
					return;
				}
			}
			i++;
			return random_piece(i);
		}
		
		public function get scroll_speed():int
			{ return _scroll_speed; }
		public function set scroll_speed(i:int):void
			{ _scroll_speed = i; recalculate_speed(); }
		public function get scroll_multi():Number
			{ return _scroll_multi; }
		public function set scroll_multi(i:Number):void
			{ _scroll_multi = i; recalculate_speed(); }
		
		protected function recalculate_speed():void {
			setAll('velocity', new FlxPoint(0, _scroll_speed*_scroll_multi));
		}
		
	}

}
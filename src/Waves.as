package  
{
	
	import org.flixel.*;
	
	import waves.*;

	/**
     * Waves - фабрика объектов wave.
     * Генерирует волны разного типа.
     * Отдельная волна имеет набор юнитов и характер их передвижения.
     * У волны есть множитель её сложности.
     * Он влияет на скорость юнитов и на состав.
     *
	 * @author Cemenyaveу и 
	 *
	 */
	public class Waves extends FlxGroup
	{
		protected var typesOfWaves:Array;
		public var currentWave:Array; //массив пачек
		protected var difficulty:int = 1;
		protected var currentEnemyPack:EnemyPacks;

        /**
         *
         */
		public function Waves()
		{
			typesOfWaves = new Array();
			typesOfWaves.push(LinarLeftToRightWave);
			typesOfWaves.push(CrossWave);
			typesOfWaves.push(BossWave);
			
			currentWave = new Array();
			
			super();
		}

        /**
         *
         *
         */
		public function startNextWave():Waves
		{
			for (var i:int = 0; i < difficulty; i++ ){
				var multiplyer:int = typesOfWaves.length - 1; // без выноса расчёта множителя Math.round() глючит.
				currentWave.push(new typesOfWaves[Math.round(Math.random() * multiplyer)](difficulty));
			}
			//currentWave.push(new BossWave(difficulty));
            startNextPack();
			return this;
		}

        /**
         *
         */
        public function startNextPack():void {
            currentEnemyPack = currentWave.shift();
            currentEnemyPack.spawnUnits();
			Callbacks.addHandler(currentEnemyPack, 'end_of_pack', this.getNextPack);
            add(currentEnemyPack);
        }

        /**
         *
         * @param args
         */
		public function getNextPack(args:EnemyPacks):void {
            if(currentEnemyPack){
				Callbacks.removeHandler(currentEnemyPack, 'end_of_pack', this.getNextPack);
                remove(currentEnemyPack);
                currentEnemyPack.destroy();
            }
			if(currentWave.length == 0){
				difficulty++;
				startNextWave();
			} else {
				startNextPack();
			}
		}
	}
}
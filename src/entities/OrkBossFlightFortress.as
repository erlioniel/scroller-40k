package entities{



    import guns.*;
	import org.flixel.*;

    public class OrkBossFlightFortress extends Unit{

        public var _leftPart:Unit;
        public var _rightPart:Unit;

        /**
         *
         */
        public function OrkBossFlightFortress(){
            super();

            makeGraphic(24, 24, 0xff999999);
            health = 50;

            _leftPart = new Unit();
            _leftPart.collidable = true;
            _leftPart.health = 50;
            _leftPart.makeGraphic(32, 32, 0xffcccccc);
            _leftPart.add(new Machinegun(_leftPart));
            add(_leftPart);

            _rightPart = new Unit();
            _leftPart.collidable = true;
            _rightPart.health = 50;
            _rightPart.makeGraphic(32, 32, 0xffcccccc);
            _leftPart.add(new Machinegun(_rightPart));
            add(_rightPart);
        }

        /**
         *
         * @param spawn_x
         * @param spawn_y
         * @return
         */
        override public function spawn(spawn_x:Number = 100, spawn_y:Number = 100):Entity
        {
            _leftPart.spawn(spawn_x - 32, spawn_y);
            _rightPart.spawn(spawn_x + 32, spawn_y);
			FlxG.log("Spawn boss");
            return super.spawn(spawn_x, spawn_y);
        }

        /**
         * Выравнивает скорость частей юнита относительно скорости самого юнита.
         */
        override public function preUpdate():void
        {
            super.preUpdate();
            _leftPart.velocity = velocity;
            _rightPart.velocity = velocity;
        }

        override public function update():void {
			super.update();
			
            for each(var member:* in _leftPart.members){
                if (member is Gun) {
					FlxG.log("Try to shoot");
                    (member as Gun).shootObject(StatePlay.$.getNearestEnemy(this));
                }
            }
            for each(var gun:* in _rightPart.members){
                if(gun is Gun){
                    (gun as Gun).shootObject(StatePlay.$.getNearestEnemy(this));
                }
            }
        }

        /**
         * Если живы части юнита, то главная часть не получает повреждений.
         */
        override public function haveHurt(obj:Entity, damage:int = 0):Boolean{
            if(_rightPart.alive || _leftPart.alive){
                return false;
            } else {
                return super.haveHurt(obj,  damage);
            }
        }
	}
}

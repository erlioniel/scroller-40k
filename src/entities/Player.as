package entities
{
	import adapters.*;
	import flash.display.Bitmap;
	import guns.Flower;
	import guns.Gun;
	import guns.Machinegun;
	import guns.RocketLauncher;
	import guns.Shotgun;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;

	public class Player extends Unit
	{
		[Embed(source = "../data/unit_gunship_skars.png")] protected var ImgPlayerSkin:Class;
		
		public function Player() 
		{
			super();
			
			health = 30;
			
			maxVelocity.x = maxVelocity.y = 200;
			drag.x = drag.y = 3000;
			
			loadGraphic(ImgPlayerSkin, true, false, 26, 27);
			addAnimation("stable", [0], 5, false);
			addAnimation("left", [1,2], 5, false);
			addAnimation("right", [3, 4], 5, false);
			
			angle = 0;
			
			width = 2;
			height = 2;
			centerOffsets();
			
			_hurtFlick = true;
			_hurtFlickTime = 2;
			
			// Создание пушек
			createGuns();
		}
		
		
		// Создание пушек
		
		protected var _currentGuns:Array;
		protected var _guns:Object;
		
		private function createGuns():void {
			_currentGuns = new Array();			
			_guns = new Array();
			
			var newGun:Gun;
			
			// Simple Gun
			newGun = new Machinegun(this);
			newGun.fireRate = 75;
			newGun.speed = 300;
			newGun.setDamage(10);
			
			_guns["simple_gun"] = add(newGun);
			
			_currentGuns = ["simple_gun"];
		}
		
		override public function destroy():void
		{
			super.destroy();
			for (var i:* in _guns) {
				_guns[i] = null;
			}
			
		}
				
		override public function update():void
		{
			super.update();
			
			if (freeze) return;
			
			//MOVEMENT
			if (FlxG.keys.pressed("A") || FlxG.keys.pressed("LEFT")) { moveLeft(); if(_curAnim.name!="left")play("left"); }
			else if (FlxG.keys.pressed("D") || FlxG.keys.pressed("RIGHT")) { moveRight(); if(_curAnim.name!="right")play("right"); }
			else { acceleration.x = 0; play("stable"); }
			if (FlxG.keys.pressed("W") || FlxG.keys.pressed("UP")) moveUp();
			else if (FlxG.keys.pressed("S") || FlxG.keys.pressed("DOWN")) moveDown();
			else acceleration.y = 0;
			
			if (FlxG.keys.justPressed("ONE")) _currentGuns = ["simple_gun"];
			if (FlxG.keys.justPressed("TWO")) _currentGuns = [];
			if (FlxG.keys.justPressed("THREE")) _currentGuns = [];
			if (FlxG.keys.justPressed("FOUR")) _currentGuns = [];
			if (FlxG.keys.justPressed("FIVE")) _currentGuns = [];
			if (FlxG.keys.justPressed("SIX")) _currentGuns = [];
			if (FlxG.keys.justPressed("SEVEN")) _currentGuns = [];
			if (FlxG.keys.justPressed("EIGHT")) _currentGuns = [];
			if (FlxG.keys.justPressed("NINE")) _currentGuns = [];

			// SHOOT!
			if (FlxG.mouse.pressed())
			{
				for each(var gun:String in _currentGuns){
					_guns[gun].fire();
				}
			}
		}
		
		override protected function get hurtDamage():int 
		{
			/**
			 * TODO надо подумать над механикой столкновений юнитов
			 * Есть идея чтобы дамаг игрока от столкновений был привязан к какой-нить игровой стате типа "сила тарана"
			 */
			return 5;
		}
		
		override public function haveHurt(obj:Entity, damage:int = 0):Boolean 
		{
			if (damage > 0.3 * maxHealth)
				damage = 0.3 * maxHealth;
			return super.haveHurt(obj, damage);
		}
		
		override public function spawn(spawn_x:Number = 100, spawn_y:Number = 100):Entity 
		{
			FlxG.camera.follow(this, FlxCamera.STYLE_TOPDOWN_TIGHT);
			return super.spawn(spawn_x, spawn_y);
		}
				
		override public function kill():void {
			if(!alive) return;
			super.kill();
			FlxG.camera.shake(0.005,0.35);
			FlxG.camera.flash(0xffd8eba2,0.35);
		}
	}
}
package entities
{
	
	import adapters.*;
	
	import flash.utils.getTimer;
	
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import org.flixel.plugin.photonstorm.BaseTypes.Bullet;
	
	public class OrkBommer extends Unit
	{
		[Embed(source = "../data/bullet_rocket.png")] protected var ImgBulletRocket:Class;
		
		protected var lastShot:Number;
		
		public function OrkBommer()
		{
			super();
			
			scoreKill = 20;
			health = 10;
			
			makeGraphic(26, 27, 0xffcccccc);
			
			width = 6;
			height = 12;
			centerOffsets();
			
			lastShot = getTimer();
			
		}
		
	}
}
package entities 
{
	import guns.*;
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Cemenyave
	 */
	public class Deffkopta extends Unit
	{
		[Embed(source = "../data/unit_deffkopta_red.png")] protected var ImgUnitKopta:Class;
		
		private var gun:Gun;

		public function Deffkopta() {
			super();
			
			scoreKill = 20;
			health = 6;
			
			loadGraphic(ImgUnitKopta, true, false, 22, 16);
			addAnimation('idle', [0, 1], 8);
			
			angle = -180;
			width = 10;
			height = 10;
			centerOffsets();
			
			var arr:Array = [Machinegun, Shotgun, RocketLauncher, Flower];
			var ran:int = Math.floor(Math.random()*4);
			
			gun = add(new arr[ran](this)) as Gun;
		}
		
		override public function update():void {
			// TODO Абсолютно кривое обращение к вражескому персонажу. Должно быть что-то типа StatePlay.getNearestEnemy()
			gun.shootObject(StatePlay.$.getNearestEnemy(this));
			super.update();
		}
	}
}

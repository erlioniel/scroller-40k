package  
{
import guns.Gun;

import sfx.*;
	import plugins.*;
	import entities.*;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class Unit extends Entity
	{
		public var _ai:UnitAI;

		// X-move
		// http://forums.flixel.org/index.php?topic=5095.0
		public var beingConveyed:Number = 0;
		public var beingLifted:Boolean = false;
		
		public function Unit() 
		{
			super();
			
			immovable = false;
			collidable = true;
		}
		
		// Singletone UnitAI
		public function get ai():UnitAI {
			if (!_ai)
				_ai = Registry.$.getFreeObject(UnitAI);
			return _ai;
		}
		public function set ai(value:UnitAI):void { return; }
		
		override public function destroy():void 
		{
			super.destroy();
			if (_ai) {
				_ai.kill();
				_ai = null;
			}
		}
		
		override public function update():void 
		{
			super.update();
			
			if (_ai != null)
				_ai.update();
		}
		
		override public function preUpdate():void 
		{
			super.preUpdate();
			
			if (beingConveyed) {
				x += beingConveyed * FlxG.elapsed;
			}
		}
		
		override public function postUpdate():void 
		{
			super.postUpdate();
			
			if (beingConveyed) {
				beingConveyed = 0;
			}
			if (beingLifted) {
				beingLifted = false;
			}
		}
		
		/**
		 * OVERRIDES
		 */
		
		override protected function get hurtDamage():int 
		{
			return health;
		}
		
		override protected function animateKill():void 
		{
			SFX.play(x+width/2, y+height/2, UnitBoom);
			super.animateKill();
		}
		
		override protected function animateDamage(damage:Number):void 
		{
			SFX.play(x+width/2, y+height/2, TextDamage, "" + damage);
			super.animateDamage(damage);
		}
	}

}
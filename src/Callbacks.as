package
{
	import flash.utils.Dictionary;
	import org.flixel.FlxBasic;
	
	/**
	 * Класс реализует механизм коллбеков
	 */
	public class Callbacks
	{
		public function Callbacks() { }
		
		/**
		 * Многомерный массив с записанными событиями
		 */
		protected static var _events:Dictionary;
		
		/**
		 * Автосоздание массива
		 */
		protected static function get events():Dictionary {
			if (!_events)
				_events = new Dictionary();
			return _events;
		}
		
		/**
		 * Вызывает коллбек для объекта с аргументами
		 * @param	object				Объект для которого вызывается коллбек
		 * @param	event				Событие, которое необходимо вызвать
		 * @param	... args			Аргументы для передачи в функцию-хендлер
		 */
		public static function call(object:FlxBasic, event:String, ... args):void {
			if (!(object in events) || !(event in events[object]))
				return;
			args.unshift(object);
			for each(var eventHandler:Function in events[object][event])
				eventHandler.apply(null, args);
		}

		/**
		 * Добавляет хендлер к объекту на определенный эвент
		 * ВНИМАНИЕ! Не забывайте удалять хендлер после того как он не нужен!
		 * @param	object				Объект для которого добавить хэндлер
		 * @param	event				Событие для обработки
		 * @param	func				Функция, в которую придет вызов по событию
		 */
		public static function addHandler(object:FlxBasic, event:String, func:Function):void {
			if (!(object in events))
				events[object] = new Array();
			if (!(event in events[object]))
				events[object][event] = new Array();
			events[object][event].push(func);
		}

		/**
		 * Удаляет хендлер объекта
		 * @param	object				Объект для которого удалить хэндлер
		 * @param	event				Событие для обработки
		 * @param	func				Функция для удаления, если не задано - удалит весь хендлер
		 */
        public static function removeHandler(object:FlxBasic, event:String, func:Function = null):void {
			if (!(object in events))
				return;
			if (!(event in events[object]))
				return;
			if(func != null) {
				var index:int = events[object][event].indexOf(func);
				if (index >= 0) {
					events[object][event].splice(index, 1);
				}
			} else {
				events[object][event].splice(0);
			}
			if (events[object][event].lenght == 0)
				events[object].splice(event, 1);
			if (events[object].lenght == 0)
				events.splice(object, 1);
        }
	}
}
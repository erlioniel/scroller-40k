package org.flixel.interfaces 
{
	import org.flixel.FlxBasic;
	/**
	 * Интерфейс для групп и подобных объектов
	 * 
	 * @author Vladimir Kryukov
	 */
	public interface FlxGroupInterface 
	{
		function getMembers():Array
		function add(Object:FlxBasic):FlxBasic
		function recycle(ObjectClass:Class = null):FlxBasic
		function remove(Object:FlxBasic, Splice:Boolean = false):FlxBasic
		function replace(OldObject:FlxBasic,NewObject:FlxBasic):FlxBasic
	}
	
}